import {WildDiv} from "./wildDiv";

export enum Selection {
	None = 0,
	Some,
	All
}

interface TagListElem {
	div:HTMLElement;
	img:HTMLImageElement;
	selection:Selection
}



export class TagList extends WildDiv {

	public tags: Map<string,TagListElem>;
	headerDiv: HTMLDivElement | null = null;
	public isLeft:boolean;
	public onSelect: ((string) => void) | undefined;
	public onChange: ((string,Selection) => void) | undefined;
	public onDestroy: ((string,Selection) => void) | undefined;
	public onclickDestroy: ((string) => void) | undefined;
	public onclickSelect: ((string,Selection) => void) | undefined;

	constructor(id: number,isLeft:boolean) {
		super(id);
		this.tags = new Map<string,TagListElem>();
		this.isLeft = isLeft;
	}

	async init(div: HTMLElement, x: number, y: number, width: number, height: number) {

		super.init(div, x, y, width, height);

		this.div!.style.height = 'auto';
		this.div!.className = 'tagList';
		this.div!.style.pointerEvents='none';


		//Create header
		this.headerDiv = document.createElement('div');
		this.div!.appendChild(this.headerDiv);
		this.headerDiv.className='tagListHeader';
		this.headerDiv.style.pointerEvents='auto';


		if (this.isLeft) {
			this.headerDiv.innerText = "Tag List";
			this.headerDiv.classList.add("tagListHeaderLeft");
		}
		else {
			this.headerDiv.innerText = "Tag List";
			this.headerDiv.classList.add("tagListHeaderRight");
		}

	}

	public changeState(tagName:string, next:Selection) {
		const item = this.tags.get(tagName);
		if (item) {
			item.selection = next;
			switch (item.selection) {
				case Selection.All:
					item.img.src="data/tick-circle.svg";
					break;
				case Selection.Some:
					item.img.src="data/tick-circle-grey.svg";
					break;
				case Selection.None:
					item.img.src="data/circle.svg";
					break;
			}

		}
	}


	public addTag(tagName:string) {
		if (tagName.length==0) return;
		if (Array.from(this.tags.keys()).includes(tagName)) return;

		const tagDiv = document.createElement('div');
		tagDiv.style.pointerEvents='auto';

		tagDiv.className="tagListItem";
		tagDiv.addEventListener('pointerdown', (event) => {
			if (this.onSelect)
				this.onSelect(tagName);
		});
		let selectImg = document.createElement('img');
		selectImg.className='tagListItemImg';
		selectImg.src="data/circle.svg";
		selectImg.addEventListener('pointerdown', (event) => {
			event.stopPropagation();
			event.preventDefault(); //Prevent displaying drag interaction
			const item = this.tags.get(tagName);

			if (item) {
				if (this.onclickSelect)
				{
					this.onclickSelect(tagName,item.selection)
				}

				if (this.onChange)
					this.onChange(tagName,item.selection);
			}
		});

		let closeImg = document.createElement('img');
		closeImg.className='tagListItemImg';
		closeImg.src="data/close.svg";
		closeImg.addEventListener('pointerdown', (event) => {
			event.stopPropagation();
			event.preventDefault(); //Prevent displaying drag interaction
			if (this.onclickDestroy) {
				this.onclickDestroy(tagName);
			}
		});
		tagDiv.innerHTML=`<span class="tagListItemText" ">&nbsp${tagName}&nbsp</span>`;
		tagDiv.appendChild(selectImg);
		tagDiv.appendChild(closeImg);
		this.tags.set(tagName,{div:tagDiv,img:selectImg,selection:Selection.None});
		this.div?.appendChild(tagDiv);
	}

	public removeTag(tagName:string) {
		const item = this.tags.get(tagName);
		if (item) {
			this.div?.removeChild(item.div);
			if (this.onDestroy)
				this.onDestroy(tagName,item.selection);
			this.tags.delete(tagName);
		}
	}
}
