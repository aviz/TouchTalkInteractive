import io from 'socket.io-client';

const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);

export class WildSync {

	public id: string;
	public isMaster: boolean;
	public socketViewSync: SocketIOClient.Socket;
	public wallOffsetX: number;
	public wallOffsetY: number;
	public wallRatio:number;


	constructor(ip: string, port: string, id: string, wallOffsetX:number, wallOffsetY:number,wallratio:number) {

		console.log("WildSync");

		this.socketViewSync = io(`http://${ip}:${port}`);

		this.id = id;

		if (id === 'MASTER') {
			this.isMaster = true;
		} else {
			this.isMaster = false;
		}

		this.wallOffsetX = wallOffsetX;
		this.wallOffsetY = wallOffsetY;
		this.wallRatio = wallRatio;


		this.socketViewSync.on('Rpc', (funcToCall) => {
			const code = funcToCall.func;
			console.log("RPC " +code);
			// @ts-ignore
			window[code].apply(this, funcToCall.args);

		});

		//register to the sync server
		this.socketViewSync.emit('ID', id);
	}

	//RPC

	public rpRegister(id, rp) {
		window[id] = rp;
	}

	public rpCall(func, args) {
		if (this.isMaster)
			this.socketViewSync.emit('Rpc', {func: func, args: args});
	}

	public anyCall(msg, content) {
		if (this.isMaster)
			this.socketViewSync.emit(msg, content);
	}

	public anyRegister(msg, func) {
		this.socketViewSync.on(msg, (content) => {
			// @ts-ignore
			func(content);
		});
	}

	public log(message:string) {
			console.log("LOG to file: " +message);
			this.anyCall('LOG',message);
	}
}

let syncServerIp = "127.0.0.1";
if (urlParams.has('syncServerIp')) {
	syncServerIp = <string>urlParams.get('syncServerIp');
}

let syncServerPort = "8081";
if (urlParams.has('syncServerPort')) {
	syncServerPort = <string>urlParams.get('syncServerPort');
}

let id = "MASTER";
if (urlParams.has('id')) {
	id = <string>urlParams.get('id');
}

let wallOffsetX = 0.0;
if (urlParams.has('wallOffsetX')) {
	// @ts-ignore
	wallOffsetX = +urlParams.get('wallOffsetX');
}
let wallOffsetY: number = 1;
if (urlParams.has('wallOffsetY')) {
	// @ts-ignore
	wallOffsetY = +urlParams.get('wallOffsetY');
}

let wallRatio: number = 1;
if (urlParams.has('wallRatio')) {
	// @ts-ignore
	wallRatio = +urlParams.get('wallRatio');
}


export const Sync = new WildSync(syncServerIp, syncServerPort, id, wallOffsetX, wallOffsetY,wallRatio);









