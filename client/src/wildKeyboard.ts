import Keyboard from 'simple-keyboard';
import './wildKeyboard.css';
import {Sync} from "./wildSync";
import {divs, WildDiv} from "./wildDiv";

export class WildKeyboard extends WildDiv {

	keyboard: Keyboard | null = null;
	keyboardDiv: HTMLDivElement | null = null;
	inputDiv: HTMLDivElement | null = null;
	headerDiv: HTMLDivElement | null = null;
	public classPrefix = 'KeyboardClass';
	public enterCallback: ((text: string) => void) | undefined;
	public addCallback: ((text: string) => void) | undefined;



	constructor(id: number) {
		super(id);
	}

	async load(div: HTMLDivElement, x: number, y: number, width: number, height: number, cssClass: string) {
		super.init(div, x, y, width, height);

		//Header
		this.headerDiv = document.createElement('div');
		this.headerDiv.classList.add(cssClass);
		this.div!.appendChild(this.headerDiv);
		this.headerDiv.classList.add('keyboardHeader');


		//Input
		this.inputDiv = document.createElement('div');
		this.inputDiv.className = 'keyboardInput';
		this.div!.appendChild(this.inputDiv);

		//Keyboard
		this.keyboardDiv = document.createElement('div');
		this.keyboardDiv.className = this.classPrefix + this.id;
		this.keyboardDiv.style.fontSize = '5px';
		// this.keyboardDiv.style.width = width +'px';
		// this.keyboardDiv.style.height = height - this.inputDiv.clientHeight - this.headerDiv.clientHeight + 'px';
		this.div!.appendChild(this.keyboardDiv);


		this.keyboard = new Keyboard(this.classPrefix + this.id, {
				onChange: (input) => {
					this.updateInput(input);
				},
				onKeyPress: (button) => {
					Sync.rpCall('keyboardHighlightKey', [this.id, button]);
					if (button == '{enter}') {
						if (this.enterCallback) {
							this.enterCallback(this.inputDiv!.innerText);
							this.updateInput('');
							this.keyboard!.clearInput()
						}
					} else if (button == '{clear}') {
							if (this.enterCallback) {
								this.enterCallback(this.inputDiv!.innerText);
								this.updateInput('');
								this.keyboard!.clearInput()
							}
						}
					else if (button == '{addtag}') {
						if (this.addCallback) {
							this.addCallback(this.inputDiv!.innerText);
						this.keyboard!.clearInput()

						}
					}
		},

				onKeyReleased: (button) => {
					console.log("Button released", button);
					Sync.rpCall('keyboardUnHighlightKey', [this.id, button]);
				}

			}
		);


		const layout = {
			default: [
				"A Z E R T Y U I O P",
				"Q S D F G H J K L M",
				"W X C V B N {bksp}",
				"{space}",
				"{addtag} {clear} {enter}"
			]
		};

		this.keyboard.setOptions({
			'layout': layout,
			'stopMouseDownPropagation': true,

			display:  {
				'{bksp}': '<-',
				'{clear}': 'clear',
				'{addtag}': 'add tag',
				'{space}': 'space',
				'{enter}': 'search',
			}
		});


	}

	public updateInput(content: string, forward = true) {
		this.inputDiv!.innerText = content;
		if (forward)
			Sync.rpCall('keyboardUpdateInput', [this.id, content]);
	}

	public highlight(key: string, forward = true) {
		// @ts-ignore
		this.keyboard!.getButtonElement(key)!.classList.add('hg-activeButton');
		if (forward)
			Sync.rpCall('keyboardHighlightKey', [this.id, key]);
	}

	public unHighlight(key: string, forward = true) {
		// @ts-ignore
		this.keyboard!.getButtonElement(key)!.classList.remove('hg-activeButton');
		if (forward)
			Sync.rpCall('keyboardUnHighlightKey', [this.id, key]);
	}

}

export function keyboardUpdateInput(id: number, content: string) {
	let div = divs.get(id) as WildKeyboard;
	if (div) {
		div.updateInput(content);
	} else {
		console.log("Cannot update content of keyboard ID " + id);
	}

}

Sync.rpRegister("keyboardUpdateInput", keyboardUpdateInput);


export function keyboardHighlightKey(id: number, key: string) {
	let div = divs.get(id) as WildKeyboard;
	if (div) {
		div.highlight(key);
	} else {
		console.log("Cannot highlight key " + key + " of keyboard ID " + id);
	}

}

Sync.rpRegister("keyboardHighlightKey", keyboardHighlightKey);


export function keyboardUnHighlightKey(id: number, key: string) {
	let div = divs.get(id) as WildKeyboard;
	if (div) {
		div.unHighlight(key);
	} else {
		console.log("Cannot unhighlight key " + key + " of keyboard ID " + id);
	}

}

Sync.rpRegister("keyboardUnHighlightKey", keyboardUnHighlightKey);
