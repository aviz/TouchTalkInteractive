import {Sync} from "./wildSync";

const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);


export class PieMenu {
  static svgNS = 'http://www.w3.org/2000/svg';
  static pieSliceClassName = 'PieSlice';
  static pieMenuIdName = 'PieMenu';
  static pieMenuDefaultLayer = 'Default';

  public readonly root: HTMLElement;
  public svg: SVGElement | undefined;
  private g: SVGGElement | undefined;
  private currentLayerId = PieMenu.pieMenuDefaultLayer;
  private longPressing = false;

  public fontRatio = 6;
  public strokeWidth = 1.5;
  public strokeColor = '#DDD';
  public opacity = 0.85;
  private readonly slices: Map<string, Array<PieSlice>>;
  public readonly r: number;

  constructor(r: number) {
    this.r = r;
    this.root = document.createElement('div') as HTMLElement;
    this.root.id = PieMenu.pieMenuIdName;
    this.root.style.background = 'none transparent';
	  this.root.style.position = 'absolute';
    this.g = undefined;
    this.hide();
    this.slices = new Map<string, Array<PieSlice>>();
  }

  public _addSlice(slice: PieSlice, layer: string): void {
    let slices = this.slices.get(layer);
    if (!slices) {
      slices = new Array<PieSlice>();
      this.slices.set(layer, slices);
    }
    slices.push(slice);
  }

  // No overloading in typescript...
  public addSlice(
    color: string,
    percent: number,
    text: string,
    clicked: () => void,
    layer = PieMenu.pieMenuDefaultLayer,
  ): void {
    this._addSlice(new PieSlice(color, percent, text, clicked), layer);
  }

  public init() {
    if (this.svg) this.svg.remove();
    this.svg = document.createElementNS(PieMenu.svgNS, 'svg') as SVGElement;
	this.svg.style.display = 'block'
    this.svg.style.width = String(2 * this.r + 2 * this.strokeWidth);
    this.svg.style.height = String(2 * this.r + 2 * this.strokeWidth);
    this.svg.style.userSelect = 'none';
    this.root.appendChild(this.svg);
    this.g = document.createElementNS(PieMenu.svgNS, 'g') as SVGGElement;
    this.g.setAttribute('opacity', String(this.opacity));
    this.svg.appendChild(this.g);

    let currentAngle = 0;
    for (const slice of this.slices.get(this.currentLayerId)!) {
      const path = document.createElementNS(PieMenu.svgNS, 'path') as SVGPathElement;
      const angle = slice.percent * 3.6;
      const d = this.describeArc(
        this.r + this.strokeWidth,
        this.r + this.strokeWidth,
        this.r,
        currentAngle,
        currentAngle + angle,
      );
      currentAngle += angle;
      path.setAttribute('d', d);
      path.setAttribute('fill', slice.color);
      path.setAttribute('stroke-width', String(this.strokeWidth));
      path.setAttribute('stroke', this.strokeColor);
      path.setAttribute('class', PieMenu.pieSliceClassName);
	  path.addEventListener('pointerdown', (event) => {
      event.stopPropagation();
    });
    path.addEventListener('pointerup', (event) => {
      if (!this.longPressing) {
        slice.clicked();
        event.stopPropagation();
      }
	  });
      path.id = '_' + Math.random().toString(36).substr(2, 9);
      slice.id = path.id;
      this.g.appendChild(path);
    }
    // add texts
    currentAngle = -Math.PI / 2;
    for (const slice of this.slices.get(this.currentLayerId)!) {
      const text = document.createElementNS(PieMenu.svgNS, 'text') as SVGTextElement;
      text.innerHTML = slice.text;
      currentAngle += ((slice.percent / 2) * 2 * Math.PI) / 100;
      text.setAttribute('text-anchor', 'middle');
      text.setAttribute('font-size', `${this.r / this.fontRatio}px`);
      text.setAttribute('x', String(this.r + this.strokeWidth + (Math.cos(currentAngle) * this.r) / 2));
      text.setAttribute('y', String(this.r + this.strokeWidth + (Math.sin(currentAngle) * this.r) / 2));
      text.setAttribute('pointer-events', 'none');
      text.setAttribute('fill', '#ddd');
      currentAngle += ((slice.percent / 2) * 2 * Math.PI) / 100;
      this.g.appendChild(text);
    }
	}

  public setCurrentLayer(layerId: string) {
    if (this.slices.get(layerId)) {
      this.currentLayerId = layerId;
      this.init();
    }
  }

  public show() {
    if (this.svg) this.svg.style.display = 'block';
  }

  public hide() {
    if (this.svg) this.svg.style.display = 'none';
  }

  public isVisible () {
	  return  (this.svg && this.svg.style.display == 'block');
  }

  public setLongPressing (value: boolean) {
    this.longPressing = value;
  }

  public isBeingPressed () {
    return this.longPressing;
  }

  private describeArc(x: number, y: number, radius: number, startAngle: number, endAngle: number): string {
    const start = this.polarToCartesian(x, y, radius, endAngle);
    const end = this.polarToCartesian(x, y, radius, startAngle);

    const arcSweep = endAngle - startAngle <= 180 ? '0' : '1';

    return [
      'M',
      start[0],
      start[1],
      'A',
      radius,
      radius,
      0,
      arcSweep,
      0,
      end[0],
      end[1],
      'L',
      x,
      y,
      'L',
      start[0],
      start[1],
    ].join(' ');
  }

  private polarToCartesian(centerX: number, centerY: number, radius: number, angleInDegrees: number): [number,number] {
    const angleInRadians = ((angleInDegrees - 90) * Math.PI) / 180.0;

    return [centerX + radius * Math.cos(angleInRadians), centerY + radius * Math.sin(angleInRadians)];
  }
}

class PieSlice {
  public color: string;
  public percent: number;
  public text: string;
  public clicked: () => void;
  public id: string = '';

  constructor(color: string, percent: number, text: string, clicked: () => void) {
    this.color = color;
    this.percent = percent;
    this.text = text;
    this.clicked = clicked;
  }
}

let wallRatio: number = 1;
if (urlParams.has('wallRatio')) {
	// @ts-ignore
	wallRatio = +urlParams.get('wallRatio');
}

export const Menu = new PieMenu(20*wallRatio);


export function pieMenuShow(x: number, y: number) {
	Menu.root!.style.left = (x * Sync.wallRatio - Sync.wallOffsetX - Menu.r) + 'px';
	Menu.root!.style.top = (y * Sync.wallRatio - Sync.wallOffsetY - Menu.r) + 'px';
	Menu.show();
	Sync.rpCall("pieMenuShow", [x, y]);
}

Sync.rpRegister("pieMenuShow", pieMenuShow);

export function pieMenuHide() {
	Menu.hide();
	Sync.rpCall("pieMenuHide", []);
}

Sync.rpRegister("pieMenuHide", pieMenuHide);

export function pieMenuSetLayer(layerID: string) {
	Menu.setCurrentLayer(layerID);
	Sync.rpCall("pieMenuSetLayer", [layerID]);
}

Sync.rpRegister("pieMenuSetLayer", pieMenuSetLayer);
