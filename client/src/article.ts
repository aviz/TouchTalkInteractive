import {divs, WildDiv} from "./wildDiv";
import {Sync} from "./wildSync";
import {dragPointers} from "./tuio";

function removeReturnAndTrim(s:string) {
	return s.replace(/\s+/g, ' ').trim();
}

export interface iArticle {
	title: string;
	content: string;
	id:number;
	year:number;
	month:number;
	day:number;
	author:string;
}

export interface iTags {
	name:string;
	isLeft:boolean;
}

export class Article extends WildDiv{
	content: iArticle|null=null;
	public highlightColor = 'yellow';
	public backgroundColor = 'white';
	headerDiv:HTMLDivElement|null=null;
	titleDiv:HTMLDivElement|null=null;
	selectButtonLeft:HTMLDivElement|null=null;
	selectButtonRight:HTMLDivElement|null=null;
	tagsDiv:HTMLDivElement|null=null;
	contentDiv:HTMLDivElement|null=null;
	textContentDiv:HTMLDivElement|null=null;
	resizeDiv:HTMLImageElement|null=null;
	scrollDiv:HTMLDivElement|null=null;
	isExpanded = true;
	arrowImg:HTMLImageElement|null=null;
	date:Date|null=null;
	expandedHeight:number=0;
	isResizing = false;
	isScrolling = false;
	grabHeight = 0;
	fixedHeaderHeight=0;
	public tags:Array<iTags>;

	//Callbacks
	public onSelectLeft:null | ((Article) => void) = null;
	public onSelectRight:null | ((Article) => void) = null;
	public onUnselect:null | ((Article) => void) = null;

	private resizeButtonHeight=0;

	public minHeight;
	public maxHeight;

	//Select
	private touchStartPosX = 0;
	private touchStartPosY = 0;

	// Highlight
	private highlightedSentences = 0;


	constructor(id:number) {
		super(id);
		articles.push(this);
		this.tags = [];
	}

	async load(div : HTMLDivElement,x:number, y:number, width:number, height:number, fixedHeaderHeight,json:iArticle) {

		super.init(div,x,y,width,height);
		this.content = json;
		this.div!.className = 'articleContainer';

		this.fixedHeaderHeight = fixedHeaderHeight;

		//Create header
		this.headerDiv = document.createElement('div');
		this.div!.appendChild(this.headerDiv);
		this.headerDiv.className='articleHeader';


		this.date = new Date(this.content!.year, this.content!.month-1, this.content!.day, 0, 0, 0)

		this.arrowImg = document.createElement('img');
		this.arrowImg.src="data/arrow-right.svg";
		this.arrowImg.style.transform = "rotate(0.25turn)";
		this.arrowImg.className='articleHeaderImg';
		this.headerDiv.appendChild(this.arrowImg);

		this.arrowImg.addEventListener('pointerdown', (event) => {
			event.stopPropagation();
			event.preventDefault(); //Prevent displaying drag interaction
			if (this.isExpanded) {
				this.collapse();
				Sync.log("Touch UNKNOWN Close document "+this.id);
			}
			else {
				this.expand();
				Sync.log("Touch UNKNOWN Open document "+this.id);
			}
		});

		this.arrowImg.addEventListener('pointerup', (event) => {
			if (dragPointers.get(event.pointerId)) {
				console.log("dragging");
			} else {
				event.stopPropagation();
			}
		});


		//Title
		let titleDiv = document.createElement('div');
		//titleDiv.className = 'articleTitle';
		titleDiv.innerHTML = `<span class="articleID">[${this.id-1000}]</span> <span class="articleTitle">${this.content!.title}</span>`;
		if (this.date)
			titleDiv.innerHTML += `&emsp;<span class="articleTitleDate">(${this.date.toDateString()})</span>`;
		titleDiv.innerHTML += `&emsp;<span class="articleTitleTags"></span>`;

		// titleDiv.style.pointerEvents="none";
		titleDiv.style.userSelect= 'none';
		this.headerDiv.appendChild(titleDiv);
		this.titleDiv = titleDiv;

		//Select divs
		let selectButtonsDiv = document.createElement('div');
		this.selectButtonLeft = document.createElement('div');
		this.selectButtonRight = document.createElement('div');


		selectButtonsDiv.style.width = fixedHeaderHeight*2+'px';
		selectButtonsDiv.style.height = fixedHeaderHeight+'px';

		this.selectButtonLeft.style.height = (fixedHeaderHeight - 4) + 'px';
		this.selectButtonLeft.style.width = fixedHeaderHeight + 'px';

		this.selectButtonRight.style.height = (fixedHeaderHeight - 4) + 'px';
		this.selectButtonRight.style.width = fixedHeaderHeight+'px';

		selectButtonsDiv.className = 'articleHeaderSelectContainer';

		this.selectButtonLeft.classList.add('articleHeaderSelect');
		this.selectButtonRight.classList.add('articleHeaderSelect');

		this.selectButtonLeft.classList.add('articleHeaderSelectLeft');
		this.selectButtonRight.classList.add('articleHeaderSelectRight');

		selectButtonsDiv.appendChild(this.selectButtonLeft);
		selectButtonsDiv.appendChild(this.selectButtonRight);

		this.headerDiv.appendChild(selectButtonsDiv);

		titleDiv.style.width = (width - fixedHeaderHeight*2 - fixedHeaderHeight - 2)+'px';


		this.selectButtonLeft.addEventListener('pointerdown',(event) => {
			event.stopPropagation();
		} );

		this.selectButtonLeft.addEventListener('pointerup',(event) => {
			if (this.isSelectedRight()) {
				//Unselect
				this.unSelect();
				this.selectLeft();
				Sync.log("Touch GREEN Select document "+this.id);
			} else if (this.isSelectedLeft()) {
				this.unSelect();
				Sync.log("Touch GREEN Deselect document "+this.id);
			} else {
				this.selectLeft();
				Sync.log("Touch GREEN Select document "+this.id);
			}

		} );

		this.selectButtonRight.addEventListener('pointerdown',(event) => {
			event.stopPropagation();
		} );

		this.selectButtonRight.addEventListener('pointerup',(event) => {
			if (this.isSelectedLeft()) {
				//Unselect
				this.unSelect();
				this.selectRight();
				Sync.log("Touch BLUE Select document "+this.id);
			} else if (this.isSelectedRight()) {
				this.unSelect();
				Sync.log("Touch BLUE Deselect document "+this.id);
			} else {
				this.selectRight();
				Sync.log("Touch BLUE Select document "+this.id);
			}

		} );


		//Size of the right bar containing resize button and scroller
		const stripwidth = width/10;

		//Create content
		this.contentDiv = document.createElement('div');
		this.contentDiv.className='articleContent';
		this.contentDiv.style.overflow='hidden';
		this.contentDiv!.addEventListener('pointerdown', (event) => {
			event.stopPropagation();
		});
		this.contentDiv!.addEventListener('pointerup', (event) => {
			if (dragPointers.get(event.pointerId)) {
				console.log("dragging");
			} else {
				event.stopPropagation();
			}
		});

		this.div!.appendChild(this.contentDiv);
		this.contentDiv.style.width ='100%';
		this.contentDiv.style.height = height-titleDiv.clientHeight+'px';

		this.contentDiv.style.position = 'absolute';

		this.textContentDiv = document.createElement('div');
		this.textContentDiv.style.position = 'absolute';
		this.textContentDiv.className='articleTextContent';
		this.contentDiv.appendChild(this.textContentDiv);
		this.textContentDiv.style.height = (this.headerDiv.clientHeight-height)+'px';

		this.textContentDiv.style.width = (width-stripwidth) + 'px';
		this.textContentDiv.style.userSelect= "none";


		//insert spans
		let contentHtml =  this.content!.content;
		contentHtml = contentHtml.replaceAll('.',`.</span><span style="background:${this.backgroundColor}";>`);
		contentHtml = `<span style="background:${this.backgroundColor}";>`+contentHtml+'</span>';
		this.textContentDiv!.innerHTML = contentHtml;
		this.div!.style.fontSize = 3.0 +"px";


		this.textContentDiv!.addEventListener('pointerdown', (event) => {
			event.stopPropagation();
			this.findClickedDiv(event.clientX,event.clientY);
		});


		this.resizeDiv = document.createElement('img');
		this.resizeDiv.src="data/arrow-resize.svg";

		this.resizeDiv.className = 'articleContentResize'
		this.contentDiv.appendChild(this.resizeDiv);
		let resizeButtonWidth =  stripwidth;
		this.resizeButtonHeight =  stripwidth;


		this.resizeDiv.style.position = 'absolute';
		this.resizeDiv.style.width = resizeButtonWidth +'px';
		this.resizeDiv.style.height = this.resizeButtonHeight +'px';
		this.resizeDiv.style.left = (width-resizeButtonWidth) + 'px';
		this.resizeDiv.style.top = (this.contentDiv.clientHeight-this.resizeButtonHeight) + 'px';

		//Resize Drag n drop
		this.resizeDiv.addEventListener('pointerdown', (event) => {
			if (this.isMoving || this.isScrolling) return;
			event.stopPropagation();
			event.preventDefault();
			this.isResizing = true;
			this.grabOffsetX = event.clientX;
			this.grabOffsetY = event.clientY;
			this.pointerId = event.pointerId;
			this.putFront();
			this.isPinned = true;
			this.grabHeight = this.div!.clientHeight;
		});

		window.addEventListener('pointermove', (event: Event)=> {
			const pEvent = (<PointerEvent>event);
			if (pEvent.pointerId == this.pointerId && this.isResizing) {
				event.preventDefault(); //Prevent displaying drag interaction
				let newHeight = this.grabHeight + pEvent.clientY - this.grabOffsetY;
				if (newHeight < this.minHeight)
					newHeight = this.minHeight;
				// this.grabOffsetX = pEvent.clientX;
				// this.grabOffsetY = pEvent.clientY;
				this.resize(this.div!.clientWidth , newHeight );
				this.updateRightStrip(true);
			}
		});

		const stopResizing = (event: PointerEvent) => {
			if (event.pointerId == this.pointerId && this.isResizing) {
				Sync.log("Touch UNKNOWN Resized document " + this.id);
				event.stopPropagation();
				this.pointerId = -1;
				this.isPinned = false;
				this.isResizing = false;
			}
		}


		window.addEventListener('pointerup', stopResizing);
		this.resizeDiv.addEventListener('pointerup', stopResizing);

		this.scrollDiv = document.createElement('div');
		this.scrollDiv.className = 'articleContentScroll'
		this.contentDiv.appendChild(this.scrollDiv);
		let scrollButtonWidth =  stripwidth;
		let scrollButtonHeight =  stripwidth*2;

		this.scrollDiv.style.position = 'absolute';
		this.scrollDiv.style.width = scrollButtonWidth +'px';
		this.scrollDiv.style.height = scrollButtonHeight +'px';
		this.scrollDiv.style.top =  '0px';
		this.scrollDiv.style.left = (width-resizeButtonWidth) + 'px';

		this.scrollDiv.addEventListener('pointerdown', (event) => {
			if (this.isMoving || this.isResizing) return;
			event.stopPropagation();
			this.isScrolling = true;
			this.grabOffsetX = event.clientX;
			this.grabOffsetY = event.clientY;
			this.pointerId = event.pointerId;
			this.putFront();
			this.isPinned = true;
		});

		window.addEventListener('pointermove', (event) => {
			const pEvent = (<PointerEvent>event);
			if (pEvent.pointerId == this.pointerId && this.isScrolling) {
				dragPointers.set(pEvent.pointerId,true);
				event.preventDefault(); //Prevent browser from scrolling
				let newPosY = this.scrollDiv!.offsetTop + pEvent.clientY - this.grabOffsetY;
				if (newPosY <0 ) newPosY = 0;
				const maxPosY = this.contentDiv!.clientHeight - this.scrollDiv!.clientHeight - this.resizeDiv!.clientHeight;
				if (newPosY > maxPosY)
					newPosY = maxPosY;

				this.grabOffsetX = pEvent.clientX;
				this.grabOffsetY = pEvent.clientY;
				//this.scrollDiv!.style.top = newPosY + 'px';

				let percentScroll = newPosY/maxPosY;

				this.scrollText(percentScroll);

			}
		});


		const stopScrolling = (event: PointerEvent) => {
			if (event.pointerId == this.pointerId) {
				if (this.isScrolling) {
					Sync.log("Touch UNKNOWN stopped scrolling Document " + this.id);
					dragPointers.set(event.pointerId,false);
					this.pointerId = -1;
					this.isPinned = false;
					this.isScrolling = false;
					event.stopPropagation();
				}
			}
		}

		window.addEventListener('pointerup', stopScrolling);
		this.scrollDiv.addEventListener('pointerup', stopScrolling);

		this.updateRightStrip(false);
		this.collapse(true);
	}

	public updateRightStrip(forward=true) {
		this.contentDiv!.style.height = this.div!.clientHeight-this.headerDiv!.clientHeight+'px';

		this.resizeDiv!.style.top = (this.contentDiv!.clientHeight-this.resizeButtonHeight) + 'px';
		this.scrollText(0,false);
		this.scrollDiv!.style.top = '0px';
		//text overflow ?
		const textOverflowHeight = this.textContentDiv!.clientHeight - this.contentDiv!.clientHeight;
		if (textOverflowHeight < 0) {
			this.scrollDiv!.style.display='none';
		}
		else {
			this.scrollDiv!.style.display='block';
		}
		if (forward)
			Sync.rpCall('updateRightStripArticle',[this.id]);

	}

	private changeArrow(marking = true){
		if (marking) {
			if (this.highlightedSentences < 1) {
				// When any sentence is highlighted, change color of arrow to yellow
				this.arrowImg!.src="data/arrow-highlighted-pilot.svg";
			}
			this.highlightedSentences = this.highlightedSentences + 1;
		} else {
			this.highlightedSentences = this.highlightedSentences - 1;

			if (this.highlightedSentences < 1) {
				// TODO: If there is no sentence highlighted anymore, arrow becomes white again
				this.arrowImg!.src="data/arrow-right.svg";
			}
		}
	}

	public switchSpanBackground (spanIndice:number,forward=true) {
		let span = this.textContentDiv!.childNodes[spanIndice] as HTMLSpanElement;
		if (span.style.background==this.highlightColor) {
			span.style.background = this.backgroundColor;
			this.changeArrow(false);
			Sync.log("Touch UNKNOWN Clear sentence '" + removeReturnAndTrim(span.innerText) + "' in document " + this.id);
		}
		else {
			span.style.background=this.highlightColor;
			Sync.log("Touch UNKNOWN Highlight sentence '" + removeReturnAndTrim(span.innerText) + "' in document " + this.id);
			this.changeArrow(true);
		}

		if (forward)
			Sync.rpCall('switchArticle',[this.id,spanIndice]);

	}

	// Methods based on switchSpanBackground but for voice commands
	public highlightSpan (spanIndice:number,forward=true) {
		let span = this.textContentDiv!.childNodes[spanIndice] as HTMLSpanElement;
		if (span.style.background==this.highlightColor) {
			console.log("sentence was already highlighted")
		}
		else {
			span.style.background=this.highlightColor;
			this.changeArrow(true);
		}

		if (forward)
			Sync.rpCall('highlightArticleVoice',[this.id,spanIndice]);
	}

	public clearSpan (spanIndice:number,forward=true) {
		let span = this.textContentDiv!.childNodes[spanIndice] as HTMLSpanElement;
		if (span.style.background==this.highlightColor) {
			span.style.background = this.backgroundColor;
			this.changeArrow(false);
		}
		else {
			console.log("sentence was already cleared")
		}

		if (forward)
			Sync.rpCall('clearArticleVoice',[this.id,spanIndice]);
	}

	private findClickedDiv(x:number,y:number) {

		let clickedElement = document.elementFromPoint(
			x,
			y
		) as HTMLSpanElement;
		let isSpan = clickedElement instanceof HTMLSpanElement;
		if (isSpan) {
			let i = 0;

			for (const childNode of clickedElement.parentElement!.childNodes) {
				if (childNode == clickedElement) {
					break;
				}
				i++;
			}
			this.switchSpanBackground(i);

			return true;
		}
		return false;
	}

	private replaceInDiv(div:HTMLDivElement, searchWord:string, replaceWord:string) {
		const regEx = new RegExp("(" + searchWord + ")(?!([^<]+)?>)", "gi");
		div.innerHTML = div.innerHTML.replace(regEx, replaceWord);
	}

	private findClickedWord(x, y) {
		let range = document.createRange();
		let words = this.div!.textContent!.split('.');
		let start = 0;
		let end = 0;
		for (let i = 0; i < words.length; i++) {
			let word = words[i];
			end = start+word.length;
			range.setStart(this.div!.firstChild!, start);
			range.setEnd(this.div!.firstChild!, end);
			// not getBoundingClientRect as word could wrap
			let rects = range.getClientRects();
			let clickedRect = this.isClickInRects(rects,x,y);
			if (clickedRect) {
				const span = document.createElement("span");
				span.style.background = "#0f6";
				return [word, start, clickedRect];
			}
			start = end + 1;
		}
		return null;
	}

	private isClickInRects(rects, x, y) {
		for (var i = 0; i < rects.length; ++i) {
			var r = rects[i]
			if (r.left<x && r.right>x && r.top<y && r.bottom>y) {
				return r;
			}
		}
		return false;
	}

	public collapse (forward=true) {
		if (!this.isExpanded) return; //Already collapsed
		this.expandedHeight = this.div!.clientHeight;
		this.div!.style.height = this.fixedHeaderHeight + 'px';
		this.isExpanded = false;
		this.arrowImg!.style.transform = "rotate(0turn)";
		if (forward)
			Sync.rpCall('collapseArticle',[this.id]);

	}

	public expand (forward=true) {
		if (this.isExpanded) return; //Already expanded
		this.div!.style.height=this.expandedHeight + 'px';
		this.contentDiv!.hidden = false;
		this.isExpanded = true;
		this.arrowImg!.style.transform = "rotate(0.25turn)";
		this.putFront();
		if (forward)
			Sync.rpCall('expandArticle',[this.id]);
	}

	public findHighlightColor(colors:Array<string>) : Array<string>{
		const foundColors = new Array<string>();
		for (const color of colors) {
			if (this.textContentDiv!.innerHTML.search(`<span style="background:${color}">`) >= 0)
				foundColors.push(color);
			else if (this.titleDiv!.innerHTML.search(`<span style="background:${color}">`) >= 0)
				foundColors.push(color);
		}

		return foundColors;
	}

	public unHighlight (color:string,forward=true) {
		this.textContentDiv!.innerHTML = this.textContentDiv!.innerHTML.replaceAll(`<span style="background:${color}">`,'')
		this.textContentDiv!.innerHTML = this.textContentDiv!.innerHTML.replaceAll(`</span style="background:${color}">`,'')

		const titleText = this.titleDiv!.querySelector('.articleTitle') as HTMLSpanElement;
		titleText!.textContent = this.content!.title; // replace the title by its standard text form

		this.titleDiv!.innerHTML = this.titleDiv!.innerHTML.replaceAll(`<span style="background:${color}">`,'')

		if (forward)
			Sync.rpCall('unHighlightArticle',[this.id,color]);

	}

	public highlight (words:Array<string>,color:string,forward=true) {
		for (const word of words) {
			this.replaceInDiv(this.textContentDiv!,word,`<span style="background:${color}">${word}</span style="background:${color}">`);
			this.replaceInDiv(this.titleDiv!,word,`<span style="background:${color}">${word}</span style="background:${color}">`);
		}
		if (forward)
			Sync.rpCall('highlightArticle',[this.id,words,color]);
	}

	public selectLeft(forward=true) {
		if (! this.div!.classList.contains('selectedLeft')) {
			this.div!.classList.add('selectedLeft');
		}
		if (! this.selectButtonLeft!.classList.contains('articleHeaderSelectLeftSelected')) {
			this.selectButtonLeft!.classList.add('articleHeaderSelectLeftSelected');
		}

		if (this.onSelectLeft) {
			this.onSelectLeft(this);
		}
		if (forward)
			Sync.rpCall('selectArticleLeft',[this.id]);
	}

	public selectRight(forward=true) {

		if (! this.div!.classList.contains('selectedRight')) {
			this.div!.classList.add('selectedRight');
		}
		if (! this.selectButtonRight!.classList.contains('articleHeaderSelectRightSelected')) {
			this.selectButtonRight!.classList.add('articleHeaderSelectRightSelected');
		}

		if (this.onSelectRight) {
			this.onSelectRight(this);
		}

		if (forward)
			Sync.rpCall('selectArticleRight',[this.id]);
	}

	public unSelect(forward =true) {
		this.div!.classList.remove('selectedLeft');
		this.div!.classList.remove('selectedRight');
		this.selectButtonLeft!.classList.remove('articleHeaderSelectLeftSelected');
		this.selectButtonRight!.classList.remove('articleHeaderSelectRightSelected');
		if (this.onUnselect) {
			this.onUnselect(this);
		}
		if (forward)
			Sync.rpCall('unSelectArticle',[this.id]);

	}

	private updateTagList() {
		//get span
		let span = this.titleDiv!.getElementsByClassName('articleTitleTags')[0];
		span.innerHTML = '';
		for (const tag of this.tags) {
			if (tag.isLeft) {
				span.innerHTML += '<mark class="articleTagLeft">#'+tag.name + ' '+'</mark>';
			} else {
				span.innerHTML += '<mark class="articleTagRight">#'+tag.name + ' '+'</mark>';
			}
		}
	}

	public isTagged(tagName:string):boolean {
		for (const tag of this.tags) {
			if (tag.name == tagName)
				return true;
		}
		return false;
	}

	public fromMonth(themonth:number):boolean {

		if (themonth == this.date!.getMonth())
			return true;
		return false;
	}

	public addTag(tagName:string, isLeft:boolean,forward =true) {

		for (const tag of this.tags) {
			if (tag.name == tagName) return;
		}

		this.tags.push({name:tagName,isLeft:isLeft});

		this.updateTagList();
		if (forward)
			Sync.rpCall('addArticleTag',[this.id,tagName,isLeft]);

	}

	public removeTag(tagName:string, isLeft:boolean, forward =true) {

		let redraw = false;
		for (let i=0;i<this.tags.length;i++) {
			if (this.tags[i].isLeft == isLeft && this.tags[i].name == tagName) {
				this.tags.splice(i,1);
				redraw=true;
				break;
			}
		}
		if (redraw)
			this.updateTagList();
		if (forward)
			Sync.rpCall('removeArticleTag',[this.id,tagName,isLeft]);

	}

	public isSelectedLeft() {
		return this.div!.classList.contains('selectedLeft');
	}

	public isSelectedRight() {
		return this.div!.classList.contains('selectedRight');
	}

	public scrollText(percent:number, forward=true) {
		if (percent < 0) percent = 0;
		if (percent > 1) percent = 1;
		const textOverflowHeight = this.textContentDiv!.clientHeight - this.contentDiv!.clientHeight
		if (textOverflowHeight > 0 ) {
			this.textContentDiv!.style.top = (-percent * textOverflowHeight) + 'px';
			const maxSliderPosY = this.contentDiv!.clientHeight - this.scrollDiv!.clientHeight - this.resizeDiv!.clientHeight;
			this.scrollDiv!.style.top = maxSliderPosY * percent + 'px';
		}
		if (forward)
			Sync.rpCall('scrollTextArticle',[this.id,percent]);

	}

	public getScrollPercent():number {
		const textOverflowHeight = this.textContentDiv!.clientHeight - this.contentDiv!.clientHeight
		if (textOverflowHeight > 0 ) {
			return -parseInt(this.textContentDiv!.style.top.replace('px','')) / textOverflowHeight;
		}
		else return -1; //Can't scroll
	}

	// check if the article has no highlighted sentences
	public isArticleClear() {
		return (this.highlightedSentences < 0);
	}

}

export let articles = new Array<Article>();



export function switchArticle (id:number,spanIndice:number) {
	let div = divs.get(id) as Article;
	if (div) {
		div.switchSpanBackground(spanIndice);
	} else {
		console.error("Cannot Show switch Div ID " + id + " span indice " + spanIndice);
	}

}
Sync.rpRegister("switchArticle",switchArticle);

export function highlightArticleVoice (id:number,spanIndice:number) {
	let div = divs.get(id) as Article;
	if (div) {
		div.highlightSpan(spanIndice);
	} else {
		console.error("Cannot hightlight Div ID " + id + " span indice " + spanIndice);
	}

}
Sync.rpRegister("highlightArticleVoice",highlightArticleVoice);

export function clearArticleVoice (id:number,spanIndice:number) {
	let div = divs.get(id) as Article;
	if (div) {
		div.clearSpan(spanIndice);
	} else {
		console.error("Cannot clear Div ID " + id + " span indice " + spanIndice);
	}

}
Sync.rpRegister("clearArticleVoice",clearArticleVoice);


export function expandArticle (id:number) {
	let div = divs.get(id) as Article;
	if (div) {
		div.expand();
	} else {
		console.error("Cannot expand switch Div ID " + id );
	}

}
Sync.rpRegister("expandArticle",expandArticle);

export function collapseArticle (id:number) {
	let div = divs.get(id) as Article;
	if (div) {
		div.collapse();
	} else {
		console.error("Cannot collapse switch Div ID " + id );
	}
}
Sync.rpRegister("collapseArticle",collapseArticle);

export function highlightArticle (id:number,words: Array<string>,color:string) {
	let div = divs.get(id) as Article;
	if (div) {
		div.highlight(words,color);
	} else {
		console.error("Cannot unHighlight on Div ID " + id );
	}

}
Sync.rpRegister("highlightArticle",highlightArticle);

export function unHighlightArticle (id:number,color:string) {
	let div = divs.get(id) as Article;
	if (div) {
		div.unHighlight(color);
	} else {
		console.error("Cannot unHighlight on Div ID " + id );
	}

}
Sync.rpRegister("unHighlightArticle",unHighlightArticle);


export function selectArticleLeft (id:number) {
	let div = divs.get(id) as Article;
	if (div) {
		div.selectLeft();
	} else {
		console.error("Cannot left select  Div ID " + id );
	}
}
Sync.rpRegister("selectArticleLeft",selectArticleLeft);


export function selectArticleRight (id:number) {
	let div = divs.get(id) as Article;
	if (div) {
		div.selectRight();
	} else {
		console.error("Cannot right select  Div ID " + id );
	}
}
Sync.rpRegister("selectArticleRight",selectArticleRight);

export function unSelectArticle (id:number) {
	let div = divs.get(id) as Article;
	if (div) {
		div.unSelect();
	} else {
		console.error("Cannot unSelect Div ID " + id );
	}

}
Sync.rpRegister("unSelectArticle",unSelectArticle);


export function scrollTextArticle (id:number,percent:number) {
	let div = divs.get(id) as Article;
	if (div) {
		div.scrollText(percent);
	} else {
		console.error("Cannot scroll on Div ID " + id );
	}

}
Sync.rpRegister("scrollTextArticle",scrollTextArticle);


export function updateRightStripArticle (id:number) {
	let div = divs.get(id) as Article;
	if (div) {
		div.updateRightStrip();
	} else {
		console.error("Cannot updateRightStrip on Div ID " + id );
	}

}
Sync.rpRegister("updateRightStripArticle",updateRightStripArticle);


const scrollStep = 0.2;

export function scrollTextArticleUp (id:number) {
	let div = divs.get(id) as Article;
	if (div) {
		console.log("Scroll Up article " + id);
		let currentScrollPercent = div.getScrollPercent();
		if ( currentScrollPercent!= -1)
		{
			div.scrollText(currentScrollPercent-scrollStep);
		}
	} else {
		console.error("Cannot scroll up on article ID " + id );
	}
}
Sync.rpRegister("scrollTextArticleUp",scrollTextArticleUp);

export function scrollTextArticleDown (id:number) {
	let div = divs.get(id) as Article;
	if (div) {
		console.log("Scroll Down article " + id);
		let currentScrollPercent = div.getScrollPercent();
		if ( currentScrollPercent!= -1)
		{
			div.scrollText(currentScrollPercent+scrollStep);
		}
	} else {
		console.error("Cannot scroll down on article ID " + id );
	}
}
Sync.rpRegister("scrollTextArticleDown",scrollTextArticleDown);

//TODO
export function hightlightSentence (id:number,sentenceNumber:number) {
	let div = divs.get(id) as Article;
	if (div) {
		console.log("hightlight sentence "+sentenceNumber+ " for article " + id);
		div.highlightSpan(sentenceNumber-1); // -1 because it is an index
	} else {
		console.error("Cannot hightlight sentence "+ sentenceNumber + " on article ID " + id );
	}
}
Sync.rpRegister("hightlightSentence",hightlightSentence);

export function unHightlightSentence (id:number,sentenceNumber:number) {
	let div = divs.get(id) as Article;
	if (div) {
		console.log("unhightlight sentence "+sentenceNumber+ " for article " + id);
		div.clearSpan(sentenceNumber-1); // -1 because it is an index
	} else {
		console.error("Cannot unhightlight sentence "+ sentenceNumber + " on article ID " + id );
	}
}
Sync.rpRegister("unHightlightSentence",unHightlightSentence);

//TODO
export function unHightlightArticle (id:number) {
	let div = divs.get(id) as Article;
	if (div) {
		console.log("unhightlight article " + id);
		// clear all the sentences of the article
		let i = 0;
		for (const childNode of div.textContentDiv!.childNodes) {
			div.clearSpan(i);
			if (div.isArticleClear()) { // no more sentences to clear
				break;
			}
			i++;
		}
	} else {
		console.error("Cannot unhightlight article ID " + id );
	}
}
Sync.rpRegister("unHightlightArticle", unHightlightArticle);


let resizeStep = 20;
export function resizeArticleBigger (id:number) {
	let div = divs.get(id) as Article;
	if (div) {
		console.log("Resize bigger article " + id);
		let newHeight = div.expandedHeight + resizeStep;
		if (newHeight > div.maxHeight) {
			newHeight = div.maxHeight;
		}
		div.expandedHeight = newHeight;

		if (!div.isExpanded)
			div.expand();
		div.resize(div.div!.clientWidth,newHeight);
		div.updateRightStrip();

	} else {
		console.error("Cannot resize bigger article ID " + id );
	}
}
Sync.rpRegister("resizeArticleBigger",resizeArticleBigger);

export function resizeArticleSmaller (id:number) {
	let div = divs.get(id) as Article;
	if (div) {
		console.log("Resize smaller article " + id);
		let newHeight = div.expandedHeight - resizeStep;
		if (newHeight < div.minHeight) {
			newHeight = div.minHeight;
		}
		div.expandedHeight = newHeight;

		if (!div.isExpanded)
			div.expand();
		div.resize(div.div!.clientWidth,newHeight);
		div.updateRightStrip();
	} else {
		console.error("Cannot resize smaller article ID " + id );
	}
}
Sync.rpRegister("resizeArticleSmaller",resizeArticleSmaller);


export function addArticleTag(id:number,tagName:string,isLeft:boolean) {
	let div = divs.get(id) as Article;
	if (div) {
		div.addTag(tagName,isLeft);
	} else {
		console.error("Cannot add Tag to Div ID " + id );
	}
}
Sync.rpRegister("addArticleTag",addArticleTag);

export function removeArticleTag(id:number,tagName:string,isLeft:boolean) {
	let div = divs.get(id) as Article;
	if (div) {
		div.removeTag(tagName,isLeft);
	} else {
		console.error("Cannot remove Tag to Div ID " + id );
	}
}
Sync.rpRegister("removeArticleTag",removeArticleTag);
