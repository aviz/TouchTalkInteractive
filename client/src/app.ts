import {divs, MoveDiv, WildDiv} from "./wildDiv";
import './app.css';


import {Article, articles} from './article';
import {Image} from './image';
import {Visualization} from './visualization';
import {Tuio} from "./tuio";
import {WildKeyboard} from "./wildKeyboard";
import {Selection, TagList} from "./tagList";
import {Sync} from "./wildSync";
import {Menu, PieMenu, pieMenuHide, pieMenuSetLayer, pieMenuShow} from "./PieMenu";


const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);


console.log(`url parameters ${urlParams}`);



let tuioServerIp = "127.0.0.1";
if (urlParams.has('tuioServerIp')) {
	tuioServerIp = <string>urlParams.get('tuioServerIp');
}

let tuioServerPort = "8083";
if (urlParams.has('tuioServerPort')) {
	tuioServerPort = <string>urlParams.get('tuioServerPort');
}

let canvasWidth: number = 1440.0;
let canvasHeight: number = 480.0;

if (urlParams.has('canvasWidth')) {
	// @ts-ignore
	canvasWidth = +urlParams.get('canvasWidth');
}
if (urlParams.has('canvasHeight')) {
	// @ts-ignore
	canvasHeight = +urlParams.get('canvasHeight');
}

let wallWidth: number = 1440.0;
let wallHeight: number = 480.0;

if (urlParams.has('wallWidth')) {
	// @ts-ignore
	wallWidth = +urlParams.get('wallWidth');
}
if (urlParams.has('wallHeight')) {
	// @ts-ignore
	wallHeight = +urlParams.get('wallHeight');
}


const ARTICLE_WIDTH = 190;
const ARTICLE_HEIGHT = 190;
const ARTICLE_HEADER_HEIGHT = 20;
const xStartArticle = 0;
const yStartArticle = 70;
const yEndArticle = 320;
const KEYBOARD_WIDTH = 150;
const KEYBOARD_HEIGHT = 115;

const IMG_WIDTH = 96;
const xStartImage = 2 * xStartArticle;
const yStartImage = 370;

const VIS_WIDTH = 300;

const app = document.getElementById("app") as HTMLDivElement;
const articleRoot = document.createElement('div');
articleRoot.id = 'articleRoot';
const uiRoot = document.createElement('div');
uiRoot.id = 'uiRoot';
const gridRoot = document.createElement('div');
gridRoot.id = 'gridRoot';

const leftMessageId = 200;
const rightMessageId = 201;
let rightMessage:WildDiv;
let leftMessage:WildDiv;
let tagListLeft:TagList;
let tagListRight:TagList;
let rightSearch = '';
let leftSearch = '';
const leftColor = "#99FFAC"; // lighter green
const rightColor = "#A7EEFF"; // lighter blue

// sorting
const SORT_BY_DATE = "date";
const SORT_BY_TITLE = "title";
const SORT_BY_ID = "number";
const waitingFor = "Wilder is ready to listen.";
const listeningOn = "Wilder is listening...";


if (app) {
	app.style.width = `${canvasWidth}px`;
	app.style.height = `${canvasHeight}px`;
	app.className = 'canvasBackground';

	console.log(`Canvas size is ${canvasWidth}px x ${canvasHeight}px`);

	app.appendChild(gridRoot);
	app.appendChild(articleRoot);
	app.appendChild(uiRoot);

	if (Sync.isMaster)
		new Tuio(tuioServerIp,tuioServerPort,canvasWidth,canvasHeight,uiRoot.id);

	let messageHeight = wallHeight / (5*4); //1/4 of a screen
	let messageWidth = wallWidth * 4.0 / 15.0; //4 screens

	let messagePosX = wallWidth * 3.5/15.0 ;
	let messagePosY = wallHeight - messageHeight;


	leftMessage = new WildDiv(leftMessageId);

	await leftMessage.init(uiRoot,messagePosX,messagePosY,messageWidth,messageHeight);
	leftMessage.div!.className = 'messageLeft';
	leftMessage.isPinned=true;


	rightMessage = new WildDiv(rightMessageId);
	messagePosX = wallWidth * 7.5/15.0 ;
	await rightMessage.init(uiRoot,messagePosX,messagePosY,messageWidth,messageHeight);
	rightMessage.div!.className = 'messageRight';
	rightMessage.isPinned=true;

	rightMessage.setText(waitingFor,false)
	leftMessage.setText(waitingFor,false)

	//Keyboard
	const keyBoardLeft = new WildKeyboard(300);
	const keyBoardX = wallWidth/2 - KEYBOARD_WIDTH/2;
	keyBoardLeft.load(uiRoot,keyBoardX,yStartArticle,KEYBOARD_WIDTH,KEYBOARD_HEIGHT,'keyboardHeaderLeft');
	keyBoardLeft.enterCallback = ((text:string) => {
		if (text.length < 3) {
			leftMessage.setText(text + " is too short");
			Sync.log("Touch GREEN Searching for too short text " + text);
			Sync.anyCall('SEARCH',{id:'LEFT',keyword:''});
		} else {
			leftSearch = text;
			Sync.log("Touch GREEN searching " + leftSearch);
			Sync.anyCall('SEARCH',{id:'LEFT',keyword:leftSearch});
		}
	});


	const keyBoardRight = new WildKeyboard(301);
	keyBoardRight.load(uiRoot,keyBoardX,yEndArticle-KEYBOARD_HEIGHT,KEYBOARD_WIDTH,KEYBOARD_HEIGHT,'keyboardHeaderRight');
	keyBoardRight.enterCallback = ((text:string) => {
		if (text.length < 3) {
			rightMessage.setText(text + " is too short");
			Sync.log("Touch BLUE Searching for too short text " + text);
			Sync.anyCall('SEARCH',{id:'RIGHT',keyword:''});
		} else {
			rightSearch = text;
			Sync.log("Touch BLUE Searching " + rightSearch);
			Sync.anyCall('SEARCH',{id:'RIGHT',keyword:rightSearch});
		}
	});


	Sync.anyRegister('RESULT',OnResult);

	//Tag lists
	const tagHeight = 20;
	tagListLeft = new TagList(302,true);
	tagListLeft.init(uiRoot,keyBoardX,yStartArticle+KEYBOARD_HEIGHT,KEYBOARD_WIDTH,200);
	keyBoardLeft.addCallback = ((text:string) => {
		Sync.log("Touch GREEN add tag " + text);
		addNewTag(text,true);
	});
	tagListLeft.onChange = ((tagName:string,selection:Selection) => {
		switch (selection) {
			case Selection.None:
				//Untag all selected articles
				Sync.log("Touch GREEN Untag all selected articles with tag: " + tagName);
				for (const article of articles) {
					if (article.isSelectedLeft())
						article.removeTag(tagName,true);
				}
				break;
			case Selection.All:
				//tag all selected articles
				Sync.log("Touch GREEN Tag all selected articles with tag: " + tagName);
				for (const article of articles) {
					if (article.isSelectedLeft())
						article.addTag(tagName,true);
				}
				break;
		}
	});
	tagListLeft.onDestroy = ((tagName:string,selection:Selection) => {
		Sync.log("Touch GREEN remove tag " + tagName);
		//Remove on the cluster
		Sync.rpCall('removeTagFromList',[tagName,true]);
		for (const article of articles) {
			article.removeTag(tagName,true);
		}
	});
	tagListLeft.onclickDestroy = ((tagName:string) => {
		removeTagFromList(tagName,true);
	});

	tagListLeft.onSelect = ((tagName:string) => {
		Sync.log("Touch GREEN add articles to the selection with tag: " + tagName);
		for (const article of articles) {
			if (article.isTagged(tagName))
				article.selectLeft()
		}
	});

	tagListLeft.onclickSelect = ((tagName:string,selection:Selection) => {
		updateTagFromList(tagName,selection,true);
	});

	tagListRight = new TagList(303,false);
	tagListRight.init(uiRoot,keyBoardX,yEndArticle,KEYBOARD_WIDTH,200);
	keyBoardRight.addCallback = ((text:string) => {
		Sync.log("Touch BLUE add tag " + text);
		addNewTag(text,false);

	});
	tagListRight.onChange = ((tagName:string,selection:Selection) => {
		switch (selection) {
			case Selection.None:
				//Untag all selected articles
				Sync.log("Touch BLUE Untag all selected articles with tag: " + tagName);
				for (const article of articles) {
					if (article.isSelectedRight())
						article.removeTag(tagName,false);
				}
				break;
			case Selection.All:
				//tag all selected articles
				Sync.log("Touch BLUE Tag all selected articles with tag: " + tagName);
				for (const article of articles) {
					if (article.isSelectedRight())
						article.addTag(tagName,false);
				}
				break;
		}
	});
	tagListRight.onDestroy = ((tagName:string,selection:Selection) => {
		Sync.log("Touch BLUE remove tag " + tagName);
		//Remove on the cluster
		Sync.rpCall('removeTag',[tagName,false]);
		for (const article of articles) {
			article.removeTag(tagName,false);
		}
	});
	tagListRight.onclickDestroy = ((tagName:string) => {
		removeTagFromList(tagName,false);
	});

	tagListRight.onSelect = ((tagName:string) => {
		Sync.log("Touch BLUE add articles to the selection with tag: " + tagName);
		for (const article of articles) {
			if (article.isTagged(tagName))
				article.selectRight()
		}
	});

	tagListRight.onclickSelect = ((tagName:string,selection:Selection) => {
		updateTagFromList(tagName,selection,false);
	});


	//Radial menu
	Menu.addSlice('#222', 50, 'Deselect', () => {
		console.log('Deselect');
		Sync.log("Touch UNKNOWN Deselect all documents");
		unSelectAll();
		pieMenuHide();
	});
	Menu.addSlice('#222', 50, "Sort", () => {
		console.log('Sort');
		pieMenuSetLayer("sort");

	});
	Menu.addSlice('#222', 33, "Title", () => {
		console.log('Sort A-Z');
		Sync.log("Touch UNKNOWN Sort A-Z");
		sort(SORT_BY_TITLE);
		pieMenuHide();

	},'sort');
	Menu.addSlice('#222', 34, "ID", () => {
		console.log('Sort 0-9');
		Sync.log("Touch UNKNOWN Sort 0-9");
		sort(SORT_BY_ID);
		pieMenuHide();
	},'sort');
	Menu.addSlice('#222', 33, "Date", () => {
		console.log('Sort Chrono');
		Sync.log("Touch UNKNOWN Sort Chrono");
		sort(SORT_BY_DATE);
		pieMenuHide();
	},'sort');

	Menu.init();
	Menu.hide();

	//Draw the grid
	let rowStartId = 4000;
	const rowStartXStep = wallWidth/15.0;
	let rowX = 0;
	const lineThickness = 4;
	let vLabelWidth = 12;
	let vLabelHeight = 20;
	for (let i = 0; i < 7; i++) {
		let vLabel = new WildDiv(rowStartId+i+20);
		await vLabel.init(gridRoot,rowX+3,0,vLabelWidth,vLabelHeight);
		vLabel.setText(String.fromCharCode('A'.charCodeAt(0) + i) );
		vLabel.div!.className='line';
		vLabel.isPinned=true;

		if (i==3)
			rowX+=rowStartXStep*3; //Middle line
		else
			rowX+=rowStartXStep*2;

		if (i<6) {
			let vLine = new WildDiv(rowStartId+i);
			await vLine.init(gridRoot,rowX,0,lineThickness,wallHeight);
			vLine.div!.className='line';
			vLine.div!.classList.add('background');

			vLine.isPinned=true;
		}

	}

	const rowStartYStep = wallHeight/5.0;
	let rowY = 0;

	let columnStartId = 4050;
	let hLabelWidth = 12;
	let hLabelHeight = 20;

	for (let i = 0; i < 5; i++) {

		let hLabelLeft = new WildDiv(columnStartId+i+20);
		await hLabelLeft.init(gridRoot,0,rowY+rowStartYStep-hLabelHeight,hLabelWidth,hLabelHeight);
		hLabelLeft.setText(i+1+'');
		hLabelLeft.div!.className='line';
		hLabelLeft.isPinned=true;

		let hLabelRight = new WildDiv(columnStartId+i+40);
		await hLabelRight.init(gridRoot,wallWidth-hLabelWidth,rowY+rowStartYStep-hLabelHeight,hLabelWidth,hLabelHeight);
		hLabelRight.setText(i+1+'');
		hLabelRight.div!.className='line';
		hLabelRight.isPinned=true;

		rowY += rowStartYStep;

		if (i <4) {
			let hLine = new WildDiv(columnStartId+i);
			await hLine.init(gridRoot,0,rowY,wallWidth,lineThickness);
			hLine.div!.className='line';
			hLine.div!.classList.add('background');
			hLine.isPinned=true;
		}

	}

	if (Sync.isMaster) {
		let startButton = document.createElement("input");
		startButton.type = "button";
		startButton.value = "Start XP";
		document.body.appendChild(startButton);
		startButton.addEventListener("click", () => {
			Sync.log("Start Experiment");
		});

		let stopButton = document.createElement("input");
		stopButton.type = "button";
		stopButton.value = "Stop XP";
		document.body.appendChild(stopButton);
		stopButton.addEventListener("click", () => {
			Sync.log("Stop Experiment");
		});

	}
}


function userPositionToColor(position:string) {
	if (position == 'LEFT') {
		return 'GREEN';
	} else if (position == 'RIGHT') {
		return 'BLUE';
	} else return 'UNKNOWN';
}

function OnResult (json) {
	console.log(json);
	let color;

	if (json.id=='LEFT') {
		leftSearch = json.keyword;
		if (leftSearch != '') {
			leftMessage.setText(json.results.length + ' result(s) for ' + leftSearch);
		} else {
			leftMessage.setText('Search cleared.');
			Sync.log("Touch GREEN Search cleared ");

		}
		color = leftColor;
	}
	else {
		rightSearch = json.keyword;
		if (rightSearch != '') {
			rightMessage.setText(json.results.length + ' result(s) for ' + rightSearch);
		} else {
			rightMessage.setText('Search cleared.');
			Sync.log("Touch BLUE Search cleared");
		}
		color = rightColor;
	}

	//unHighlight previous search
	for (const article of articles) {
		article.unHighlight(color);
	}

	Sync.log(userPositionToColor(json.id) + " Search results " + JSON.stringify(json.results));

	//Highlight found words
	for (const jsonElement of json.results) {
		console.log(jsonElement);
		let article = divs.get(jsonElement.id) as Article;
		if (article) {
			article.highlight(jsonElement.terms,color);
		}
		else
			console.error('Cannot find article #'+jsonElement.id);
	}

	//Set header colors according to words found
	for (const article of articles) {
		const colors = article.findHighlightColor([leftColor,rightColor]);
		article.removeClass(['articleContainerLeft','articleContainerRight','articleContainerNone','articleContainerAll'])
		if (colors.length == 0)
			article.addClass(['articleContainerNone']);
		else if (colors.length == 2)
			article.addClass(['articleContainerAll']);
		else if (colors.indexOf(leftColor)>=0)
			article.addClass(['articleContainerLeft']);
		else
			article.addClass(['articleContainerRight']);
	}
}


const articlesFiles = ['1101162143775.json'
	];

const imageFiles = ['dummy.png'];

const visFiles = ['timeline-docs.svg'];

let xArticle = xStartArticle;
let yArticle = yStartArticle;
let xArticleMargin = 1;
let yArticleMargin = 1;


let xImage = xStartImage;
let imageID = 3001; // the IDs starting with 3000 are images

let visID = 5001;

async function init() {
	// Load all the news articles and additional documents
	await Promise.all(articlesFiles.map(async (path): Promise<Article> => {
		const fullPath ='data/'+path;
		console.log("Loading " + fullPath);
		const jsonFile = await fetch(fullPath);
		const json = await jsonFile.json().catch((reason)=> {
			console.log("Invalid Json " + fullPath);
		});
		let article = new Article(json.id);
		article.minHeight = ARTICLE_HEIGHT/2;
		article.maxHeight = ARTICLE_HEIGHT*2;
		if (yArticle > yEndArticle) {
			xArticle += ARTICLE_WIDTH + 4 + xArticleMargin; //Border: 2 + 2
			yArticle = yStartArticle
		}
		yArticle += 19 + yArticleMargin; //Border: 2 + 2
		await article.load(articleRoot,xArticle,yArticle,ARTICLE_WIDTH,ARTICLE_HEIGHT,ARTICLE_HEADER_HEIGHT,json);
		article.onDraggingStarted = (() => {
			if (article.isSelectedLeft() || article.isSelectedRight()) {
				let offset = 0;
				let selectedArticles = new Array<Article>();
				let sameColorSelectedArticles = new Array<Article>();
				let isSelected;
				for (const currentArticle of articles) {
					let isSelected:(number)=>boolean;
					if (article.isSelectedLeft())
						isSelected = isArticleSelectedLeft;
					else
						isSelected = isArticleSelectedRight;

					if (isSelected(currentArticle.id) && currentArticle != article) {
						currentArticle.collapse();
						selectedArticles.push(currentArticle);
					}
				}
				if (selectedArticles.length > 0)
					article.collapse();
				for (const currentArticle of selectedArticles) {
					offset += currentArticle.div!.getBoundingClientRect().height;
					currentArticle.move(article.div!.getBoundingClientRect().left,article.div!.getBoundingClientRect().top + offset);
					currentArticle.putFront();
				}
			}
		});

		article.onDragging = (() => {
			if (article.isSelectedLeft() || article.isSelectedRight()) {
				let offset = 0;
				let selectedArticles = new Array<Article>();
				for (const currentArticle of articles) {
					let isSelected:(number)=>boolean;
					if (article.isSelectedLeft())
						isSelected = isArticleSelectedLeft;
					else
						isSelected = isArticleSelectedRight;

					if (isSelected(currentArticle.id) && currentArticle != article) {
						selectedArticles.push(currentArticle);
					}
				}
				for (const currentArticle of selectedArticles) {
					offset += currentArticle.div!.getBoundingClientRect().height;
					currentArticle.move(article.div!.getBoundingClientRect().left,article.div!.getBoundingClientRect().top + offset);
				}
			}
		});

		article.onDraggingEnded = (() => {
			//Sync.log("Touch UNKNOWN Moved Document " + article.id);
			if (article.isSelectedLeft() || article.isSelectedRight()) {
				let maxOffset = article.div!.getBoundingClientRect().height;
				let selectedArticles = new Array<Article>();
				for (const currentArticle of articles) {
					let isSelected:(number)=>boolean;
					if (article.isSelectedLeft())
						isSelected = isArticleSelectedLeft;
					else
						isSelected = isArticleSelectedRight;
					if (isSelected(currentArticle.id) && currentArticle != article) {
						selectedArticles.push(currentArticle);
						maxOffset += currentArticle.div!.getBoundingClientRect().height;
					}
				}
				//Are all the articles in the app ?
				if (article.div!.getBoundingClientRect().top + maxOffset > app.getBoundingClientRect().bottom) {
					article.move(article.div!.getBoundingClientRect().left,app.getBoundingClientRect().bottom-maxOffset);
					let offset = 0;
					for (const currentArticle of selectedArticles) {
						offset += currentArticle.div!.getBoundingClientRect().height;
						currentArticle.move(article.div!.getBoundingClientRect().left,article.div!.getBoundingClientRect().top + offset);
					}
				}
				Sync.log("Touch UNKNOWN Moved selected documents including " + article.id);
			} else {
				Sync.log("Touch UNKNOWN Moved document " + article.id);
			}
		});

		article.onSelectLeft = ((article:Article) => {
			updateTagListStates(tagListLeft,true);
		})

		article.onSelectRight = ((article:Article) => {
			updateTagListStates(tagListRight,false);
		})

		article.onUnselect = ((article:Article) => {
			updateTagListStates(tagListLeft,true);
			updateTagListStates(tagListRight,false);
		})

		return article;
	}));

	// Load all the images
	await Promise.all(imageFiles.map(async (path): Promise<Image> => {
		const fullpath ='data/'+path;
		console.log("Loading " + fullpath);
		let image = new Image(imageID);
		imageID += 1;
		xImage += IMG_WIDTH + 2; //Border: 1 + 1
		await image.load(articleRoot,xImage,yStartImage,IMG_WIDTH,-1,path);
		return image;
	}));

	// Load all the visualizations
	await Promise.all(visFiles.map(async (path): Promise<Image> => {
		const fullpath ='data/'+path;
		console.log("Loading " + fullpath);
		let vis = new Visualization(visID);
		visID += 1;
		xImage += VIS_WIDTH + 2; // Put vis next to images
		await vis.load(articleRoot,xImage,yStartImage,VIS_WIDTH,-1,path);

		// Add event listeners
		vis.addMonthLabels((monthNumber:number, isLeftUser:boolean) => {
			toggleSelectArticlesWithMonth(monthNumber, isLeftUser);
		});

		return vis;
	}));
}

function updateTagListStates(tagList:TagList,isLeft:boolean) {
	//For all tags in this tag list
	for (const [tag, value] of tagList.tags.entries()) {
		let articleSelected = 0;
		let articleSelectedTagged = 0;
		for (const article of articles) {
			if (isLeft) {
				if (article.isSelectedLeft()) {
					articleSelected++;
					if (article.isTagged(tag))
						articleSelectedTagged++;
				}
			} else {
				if (article.isSelectedRight()) {
					articleSelected++;
				 	if (article.isTagged(tag))
						articleSelectedTagged++;
				}
			}
		}
		if (articleSelectedTagged == 0) {
			tagList.changeState(tag,Selection.None);
		}
		else if (articleSelectedTagged == articleSelected) {
			tagList.changeState(tag,Selection.All);
		}
		else
			tagList.changeState(tag,Selection.Some);

	}
}

function isArticleSelectedRight(articleId:number):boolean {
	for (const article of articles) {
		if (article.id == articleId) {
			if (article.isSelectedRight())
				return true;
			else
				return false;
		}
	}
	return false
}

function isArticleSelectedLeft(articleId:number):boolean {
	for (const article of articles) {
		if (article.id == articleId) {
			if (article.isSelectedLeft())
				return true;
			else
				return false;
		}
	}
	return false
}


function unSelectAll () {
	for (const article of articles) {
		article.unSelect();
	}
}

Sync.rpRegister("unSelectAll", unSelectAll);

let menuTime = -1;
let menuPos = [0,0];
let timeoutID;
const MenuLongPressTimeMinInMillis = 1000;
const MenuLongPressDistanceMaxInPixels = 4;

// Sort articles
function sort(attribute:string, group="all") {
	xArticle = xStartArticle;
	yArticle = yStartArticle;

	switch(attribute) {
		case SORT_BY_DATE: {
			articles.sort((a, b) => (a.date! < b.date! ? -1 : 1));
			break;
		}
		case SORT_BY_TITLE: {
			articles.sort((a, b) => (a.content!.title < b.content!.title ? -1 : 1));
			break;
		}
		case SORT_BY_ID: {
			articles.sort((a, b) => (a.id! < b.id! ? -1 : 1));
			break;
		}
		case "ID": {
			articles.sort((a, b) => (a.id! < b.id! ? -1 : 1));
			break;
		}
		default: {
			console.log("Invalid sorting.")
			break;
		}
	}

	// move all articles
	let column = 0;
	const columNumber = 6;
	const borderThickness = 4; //Border: 2 + 2
	let xMargin = (wallWidth - columNumber * (ARTICLE_WIDTH + borderThickness) - KEYBOARD_WIDTH) / (columNumber-1);
	for (const article of articles) {
		if (yArticle > yEndArticle) {
			xArticle += ARTICLE_WIDTH + borderThickness + xMargin;
			column++;
			if (column == columNumber/2)
				xArticle += KEYBOARD_WIDTH;
			yArticle = yStartArticle
		}
		article.move(xArticle, yArticle, true);
		yArticle += ARTICLE_HEADER_HEIGHT + yArticleMargin + borderThickness;
	}

}

Sync.rpRegister("sort", sort);

function undo () {
	console.log("Placeholder function for undo.")
}

Sync.rpRegister("undo", undo);

// Show message when the speech recognizer is actively listening
function showListen (personID) {
	if (personID=='LEFT') {
		leftMessage.setText(listeningOn)
	} else {
		rightMessage.setText(listeningOn)
	}
}

Sync.rpRegister("showListen", showListen);

// Show speech command when it is understood
function showSpeechCommand (personID, command) {
	if (personID=='LEFT') {
		leftMessage.setText(command)
	} else {
		rightMessage.setText(command)
	}
}

Sync.rpRegister("showSpeechCommand", showSpeechCommand);

// Show message when the speech recognition fails
function reportRecognitionError (personID) {
	if (personID=='LEFT') {
		leftMessage.setText("Sorry, I did not understand that.")
	} else {
		rightMessage.setText("Sorry, I did not understand that.")
	}
}

Sync.rpRegister("reportRecognitionError", reportRecognitionError);

// Show pie menu while pressing for at least 1 second
function showPieMenuOnLongPress(event) {
	Menu.setLongPressing(true);

	if (menuTime != -1) {
		pieMenuSetLayer(PieMenu.pieMenuDefaultLayer);
		pieMenuShow(event.clientX,event.clientY);
	}
}

// Start timer for showing pie menu on long press
function startLongPressTimer(event) {
	timeoutID = setTimeout(() => {
    showPieMenuOnLongPress(event);
  }, MenuLongPressTimeMinInMillis);
}

init().then(() => {

	uiRoot.insertBefore(Menu.root, uiRoot.firstChild);
	app.classList.add('Background');
	app.addEventListener('pointerdown',(event) => {
		const divAtPointerPosition = document.elementFromPoint(event.clientX, event.clientY);
		if (divAtPointerPosition && divAtPointerPosition.classList.contains('Background')) {
			console.log("MENU POINTER DOWN");
			menuTime = new Date().getTime();
			menuPos = [event.clientX,event.clientY];
			startLongPressTimer(event);
		}

	});
	app.addEventListener('pointerup',(event) => {
		clearTimeout(timeoutID);

		if (Menu.isVisible() && !Menu.isBeingPressed()) {
			pieMenuHide();
		}

		menuPos = [0,0];
		menuTime = -1;
		Menu.setLongPressing(false);
	});

	// Show articles sorted by ID
	sort(SORT_BY_ID);

});

export function Open() {

}
Sync.rpRegister("Open",Open);


export function moveArticle (id:number,columnId:string, rowId:string) {
	let div = divs.get(id) as Article;
	if (div) {
		console.log("Move article " + id + " to "+columnId + " ," + rowId );
		//'A'.charCodeAt(0)
		//Get column offset
		const columnOffset = columnId.charCodeAt(0) - 'A'.charCodeAt(0);
		if (columnOffset < 0 && columnOffset > 6) {
			console.error('moveArticle : Invalid column');
			return;
		}
		const columnXStep = wallWidth/15;
		let columnX = columnOffset * columnXStep *2;
		if (columnOffset>=3)
			columnX+=columnXStep; //Middle line

		const row =  parseInt(rowId);
		if (row < 1 && row > 5) {
			console.error('moveArticle: Invalid row');
			return;
		}

		const rowOffset = row - 1;
		const rowYStep = wallHeight/5;
		let rowY = rowOffset * rowYStep + rowYStep/2;

		MoveDiv(id,columnX,rowY);

	} else {
		console.log("Cannot move article " + id + " to "+columnId + " ," + rowId );
	}
}
Sync.rpRegister("moveArticle",moveArticle);

// Move articles the user has already selected
export function moveArticleGroup (column:string, row:string, userId:string) {

	let selectedArticles = new Array<Article>();
	const user = userId.toUpperCase();

	let messageDiv:WildDiv;
	if (user == 'LEFT')
		messageDiv = leftMessage;
	else
		messageDiv = rightMessage;

	for (const article of articles) {
		if (user == 'LEFT' && article.isSelectedLeft()) {
			selectedArticles.push(article)
		}else {
			if (user == 'RIGHT' && article.isSelectedRight())
				selectedArticles.push(article)
		}
	}

	if (selectedArticles.length > 0 ) {
		console.log("move all article to "+ column + "," + row );
		for (const selectedArticle of selectedArticles) {
			moveArticle(selectedArticle.id,column,row);
		}
		messageDiv.setText("Ok, I moved the documents to the zone " + column + row + ".", true)
	}
	else {
		messageDiv.setText("Please select one or more articles for moving.", true)
	}
}
Sync.rpRegister("moveArticleGroup",moveArticleGroup);


function addNewTag(tagName:string,isLeft:boolean) {
	const tagList:TagList = isLeft ? tagListLeft:tagListRight;

	tagList.addTag(tagName.toUpperCase());

	if (Sync.isMaster)
		Sync.rpCall('addNewTag',[tagName,isLeft]);

}
Sync.rpRegister("addNewTag",addNewTag);

function removeTagFromList(tagName:string,isLeft:boolean) {
	const tagList:TagList = isLeft ? tagListLeft:tagListRight;

	tagList.removeTag(tagName.toUpperCase());

	if (Sync.isMaster)
		Sync.rpCall('removeTagFromList',[tagName,isLeft]);
}
Sync.rpRegister("removeTagFromList",removeTagFromList);


function updateTagFromList(tagName:string, currentSelection:Selection,isLeft:boolean) {

	const tagList:TagList = isLeft ? tagListLeft:tagListRight;

	switch (currentSelection) {
		case Selection.All:
			tagList.changeState(tagName,Selection.None);
			break;
		case Selection.Some:
			tagList.changeState(tagName,Selection.All);
			break;
		case Selection.None:
			tagList.changeState(tagName,Selection.All);
			break;
	}

	if (Sync.isMaster)
		Sync.rpCall('updateTagFromList',[tagName,currentSelection,isLeft]);
}
Sync.rpRegister("updateTagFromList",updateTagFromList);

// Allow speech command to assign an existing tag to previously selected articles
export function assignTagToSelectedArticles(tagName:string,isLeft:boolean) {

	const spokenTag = tagName.toUpperCase();

	if (isLeft) {
		if(tagListLeft.onChange) {
			tagListLeft.onChange(spokenTag, Selection.All);
		} else {
			console.log("Cannot assign tag " + spokenTag + " to selected articles");
		}

	} else {
		if(tagListRight.onChange) {
			tagListRight.onChange(spokenTag, Selection.All);
		} else {
			console.log("Cannot assign tag " + spokenTag + " to selected articles");
		}
	}
	updateTagFromList(spokenTag, Selection.None, isLeft);
}
Sync.rpRegister("assignTagToSelectedArticles",assignTagToSelectedArticles);

// Allow speech command to remove a tag from selected articles
export function removeTagFromSelectedArticles(tagName:string,isLeft:boolean) {

	const spokenTag = tagName.toUpperCase();

	if (isLeft) {
		if(tagListLeft.onChange) {
			tagListLeft.onChange(spokenTag, Selection.None);
		} else {
			console.log("Cannot remove tag " + spokenTag + " from selected articles");
		}

	} else {
		if(tagListRight.onChange) {
			tagListRight.onChange(spokenTag, Selection.None);
		} else {
			console.log("Cannot remove tag " + spokenTag + " from selected articles");
		}
	}
	updateTagFromList(spokenTag, Selection.All, isLeft);
}
Sync.rpRegister("removeTagFromSelectedArticles",removeTagFromSelectedArticles);

// Allow speech command to select articles that have a given tag
export function selectArticlesWithTag(tagName:string,isLeft:boolean) {

	const spokenTag = tagName.toUpperCase();

	if (isLeft) {
		if(tagListLeft.onSelect) {
			tagListLeft.onSelect(spokenTag);
		} else {
			console.log("Cannot select articles with tag " + spokenTag);
		}

	} else {
		if(tagListRight.onSelect) {
			tagListRight.onSelect(spokenTag);
		} else {
			console.log("Cannot select articles with tag " + spokenTag);
		}
	}
	updateTagFromList(spokenTag, Selection.None, isLeft);
}
Sync.rpRegister("selectArticlesWithTag",selectArticlesWithTag);

// Allow speech command to select articles of a given month
export function selectArticlesWithMonth(monthNumber:number,isLeft=true) {

	for (const article of articles) {
		if (article.fromMonth(monthNumber))
			if (isLeft) {
				article.selectLeft()
			} else {
				article.selectRight()
			}
	}

	if (Sync.isMaster)
		Sync.rpCall('selectArticlesWithMonth', [monthNumber,isLeft]);
}
Sync.rpRegister("selectArticlesWithMonth",selectArticlesWithMonth);

// Allow speech command to deselect articles of a given month
export function unselectArticlesWithMonth(monthNumber:number) {

	for (const article of articles) {
		if (article.fromMonth(monthNumber))
				article.unSelect()
	}

	if (Sync.isMaster)
		Sync.rpCall('unselectArticlesWithMonth', [monthNumber]);
}
Sync.rpRegister("unselectArticlesWithMonth",unselectArticlesWithMonth);

// Allow toggling the selection of articles of a given month
export function toggleSelectArticlesWithMonth(monthNumber:number,isLeft=true) {

	for (const article of articles) {
		if (article.fromMonth(monthNumber))
			if (isLeft) {
				// if the article was selected by another color, swap
				if (article.isSelectedRight()) {
					//Unselect
					article.unSelect();
					article.selectLeft();
					Sync.log("Touch GREEN Select document of month " + monthNumber);
				} else {
					article.selectLeft();
					Sync.log("Touch GREEN Select document of month " + monthNumber);
				}
			} else {
				// if the article was selected by another color, swap
				if (article.isSelectedLeft()) {
					//Unselect
					article.unSelect();
					article.selectRight();
					Sync.log("Touch BLUE Select document of month " + monthNumber);
				} else {
					article.selectRight();
					Sync.log("Touch BLUE Select document of month " + monthNumber);
				}
			}
	}

	if (Sync.isMaster)
		Sync.rpCall('toggleSelectArticlesWithMonth', [monthNumber,isLeft]);
}
Sync.rpRegister("toggleSelectArticlesWithMonth",toggleSelectArticlesWithMonth);
