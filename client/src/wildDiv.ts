import {Sync} from "./wildSync";

export class WildDiv {

	div: HTMLElement | null;
	pointerId:number = -1;
	grabOffsetX : number = 0.0;
	grabOffsetY : number = 0.0;
	id: number;
	public isPinned:boolean = false;
	isMoving:boolean = false;
	public onDraggingStarted:undefined|(()=>void)=undefined;
	public onDragging:undefined|(()=>void)=undefined;
	public onDraggingEnded:undefined|(()=>void)=undefined;

	constructor(id:number) {
		this.div = null;
		this.id = id;
	}

	async init(div : HTMLElement,x:number, y:number, width:number, height:number) {
		this.div = document.createElement('div');
		this.div.style.position = 'absolute';
		this.div.id = this.id+'';
		div.appendChild(this.div);

		if (!this.isPinned) {
			//Drag n drop
			this.div.addEventListener('pointerdown' , (event) => {
				if (!this.isPinned) {
					this.isMoving = true;
					this.grabOffsetX = event.clientX - this.div!.getBoundingClientRect().left;
					this.grabOffsetY = event.clientY - this.div!.getBoundingClientRect().top;
					this.pointerId = event.pointerId;
					this.putFront();
					if (this.onDraggingStarted)
						this.onDraggingStarted();
				}
			});

			window.addEventListener('pointermove', (event) => {
				const pEvent = (<PointerEvent>event);
				if (pEvent.pointerId == this.pointerId && this.isMoving) {
					this.move(pEvent.clientX - this.grabOffsetX , pEvent.clientY - this.grabOffsetY );
					if (this.onDragging)
						this.onDragging();
				}
			});

			const stopMoving = (event: PointerEvent) => {
				if (event.pointerId == this.pointerId ) {
					if (this.isMoving) {
						this.pointerId = -1;
						this.isMoving = false;
						if (this.onDraggingEnded)
							this.onDraggingEnded();
						event.stopPropagation();
					}
				}
			}

			this.div.addEventListener('pointerup' ,stopMoving);
			window.addEventListener('pointerup' ,stopMoving);
		}

		divs.set(this.id,this);

		this.div!.style.scale=Sync.wallRatio+'';
		this.div!.style.transformOrigin ='0 0';

		this.move(x,y,false);

		this.resize(width,height,false);

	}

	public move (x:number , y: number, forward= true) {
		this.div!.style.left = x * Sync.wallRatio - Sync.wallOffsetX + 'px';
		this.div!.style.top = y * Sync.wallRatio - Sync.wallOffsetY + 'px';
		if (forward)
			Sync.rpCall('MoveDiv',[this.id,x,y]);
	}


	public resize (w:number , h: number,forward=true) {
		if (w != -1)
			this.div!.style.width = w  + 'px';
		if (h != -1)
				this.div!.style.height = h  + 'px';
		if (forward)
			Sync.rpCall('ResizeDiv',[this.id,w,h]);
	}

	public show (forward=true) {
		this.div!.hidden = false;
		if (forward)
			Sync.rpCall('ShowDiv',[this.id]);
	}

	public hide (forward=true) {
		this.div!.hidden = true;
		if (forward)
			Sync.rpCall('HideDiv',[this.id]);
	}

	public putFront (forward=true) {
		this.div!.parentElement!.insertAdjacentElement('beforeend',this.div!);
		if (forward)
			Sync.rpCall('PutFrontDiv',[this.id]);
	}

	public setText (text: string,forward=true) {
		this.div!.innerHTML = text;
		if (forward)
			Sync.rpCall('SetTextDiv',[this.id,text]);
	}

	public addClass (newClasses: Array<string>,forward=true) {
		for (const newClass of newClasses) {
			this.div!.classList.add(newClass);
		}
		if (forward)
			Sync.rpCall('AddClassDiv',[this.id,newClasses]);
	}

	public removeClass (oldClasses: Array<string>,forward=true) {
		for (const oldClass of oldClasses) {
			this.div!.classList.remove(oldClass);
		}
		if (forward)
			Sync.rpCall('RemoveClassDiv',[this.id,oldClasses]);
	}
}

export const divs = new Map <number,WildDiv>();

export function MoveDiv (id:number ,x:number , y: number) {
	let div = divs.get(id);
	if (div) {
		div.move(x ,y);
	} else {
		console.error("Cannot find div ID " + id);
	}
}
Sync.rpRegister("MoveDiv",MoveDiv);


export function ResizeDiv (id:number ,w:number , h: number) {
	let div = divs.get(id);
	if (div) {
		div.resize(w ,h);
	} else {
		console.error("Cannot find div ID " + id);
	}

}
Sync.rpRegister("ResizeDiv",ResizeDiv);

export function HideDiv (id:number) {
	let div = divs.get(id);
	if (div) {
		div.hide();
	} else {
		console.error("Cannot find Hide ID " + id);
	}

}
Sync.rpRegister("HideDiv",HideDiv);

export function ShowDiv (id:number) {
	let div = divs.get(id);
	if (div) {
		div.show();
	} else {
		console.error("Cannot Show div ID " + id);
	}

}
Sync.rpRegister("ShowDiv",ShowDiv);


export function PutFrontDiv (id:number) {
	let div = divs.get(id);
	if (div) {
		div.putFront();
	} else {
		console.error("Cannot put div ID " + id + " in front");
	}

}
Sync.rpRegister("PutFrontDiv",PutFrontDiv);

export function SetTextDiv (id:number,text: string) {
	let div = divs.get(id);
	if (div) {
		div.setText(text);
	} else {
		console.error("Cannot change text of div ID " + id);
	}

}
Sync.rpRegister("SetTextDiv",SetTextDiv);


export function AddClassDiv ( id:number,newClasses: Array<string>) {
	let div = divs.get(id);
	if (div) {
		div.addClass(newClasses);
	} else {
		console.error("Cannot change class of div ID " + id);
	}

}
Sync.rpRegister("AddClassDiv",AddClassDiv);

export function RemoveClassDiv ( id:number,oldClasses: Array<string>) {
	let div = divs.get(id);
	if (div) {
		div.removeClass(oldClasses);
	} else {
		console.error("Cannot change class of div ID " + id);
	}

}
Sync.rpRegister("RemoveClassDiv",RemoveClassDiv);
