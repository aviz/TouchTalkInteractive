import {divs, WildDiv} from "./wildDiv";
import {Sync} from "./wildSync";

export interface iImage {
	id:number;
}

export class Image extends WildDiv {
	contentImg:HTMLImageElement|null=null;

	constructor(id:number) {
		super(id);
	}

	async load(div : HTMLDivElement, x:number, y:number, width:number, height:number, url:string) {

		super.init(div, x, y, width, height);

		//Create image
		this.contentImg = document.createElement('img');
		this.contentImg.src="data/" + url;
		this.contentImg.className='dataImg';
		this.div!.appendChild(this.contentImg);
		this.contentImg.addEventListener('pointerdown', (event) => {
			event.preventDefault();
		})
	}

}
