import io from "socket.io-client";
import {divs, HideDiv, MoveDiv, ShowDiv, WildDiv} from "./wildDiv";
import { Sync } from './wildSync';

interface TuioMessage {
	xPos: number;
	yPos: number;
	cursorId: number;
}

export class Tuio {

	// Touch input
	private socketInput: SocketIOClient.Socket | null = null;
	private pointerInfos: Map<number, Element>;
	private pointersHistory: Map<number, TuioMessage>;
	private divWith:number;
	private divHeight:number;

	constructor(ip: string,port:string, divWith, divHeight,uiRootId) {

		this.divWith = divWith;
		this.divHeight = divHeight;
		this.pointerInfos = new Map<number, Element>();
		this.pointersHistory = new Map<number,TuioMessage>();


		if (Sync.isMaster) {
			this.socketInput = io('http://' + ip + ':' + port);

			this.socketInput.on('onAdd', (json: TuioMessage) => {
				let roundX = Number(json.xPos.toFixed(3));
				let roundY = Number(json.yPos.toFixed(3));

				console.log('onAdd ' +json.cursorId + ' ' + json.xPos + ' '+ json.yPos +' -> '+roundX+' '+roundY);

				if (roundX == 0.0 && roundY == 0.0) console.log('false add touch');
				else this.LaunchTouchEvent(json, 'pointerdown');
			});

			this.socketInput.on('onUpdate', (json: TuioMessage) => {
				let roundX = Number(json.xPos.toFixed(3));
				let roundY = Number(json.yPos.toFixed(3));

				//console.log('onUpdate '+ json.xPos + ' '+ json.yPos );

				if (roundX == 0.0 && roundY == 0.0) console.log('false add touch');
				else this.LaunchTouchEvent(json, 'pointermove');
			});

			this.socketInput.on('onRemove', (json: TuioMessage) => {
				let roundX = Number(json.xPos.toFixed(3));
				let roundY = Number(json.yPos.toFixed(3));

				console.log('onRemove '+json.cursorId + ' '+ json.xPos + ' '+ json.yPos +' -> '+roundX+' '+roundY);

				if (roundX == 0.0 && roundY == 0.0) {
					if (!this.pointersHistory.get(json.cursorId)) {
						console.log('false remove touch');
						return;
					}
				}
				else this.LaunchTouchEvent(json, 'pointerup');
				this.pointersHistory.delete(json.cursorId);
			});
		}

		//Prelaod touch divs
		let app = document.getElementById(uiRootId) as HTMLDivElement;
		const pointerSize = 3.0;

		// for (let i = 1; i <= 32; i++) {
		// 	let pointer = new WildDiv(i);
		// 	pointer.isPinned = true;
		// 	pointer.init(app,0,0,pointerSize,pointerSize) ;
		// 	pointer.hide(true);
		// 	pointer.div!.style.background="red";
		// }

		// //Display touch points
		// function onPointerDown (event) {
		// 	MoveDiv(event.pointerId,event.clientX-pointerSize/2,event.clientY-pointerSize/2);
		// 	ShowDiv(event.pointerId);
		// }
		//
		// window.addEventListener("pointerdown",onPointerDown);
		//
		// function onPointerMove (event) {
		// 	MoveDiv(event.pointerId,event.clientX-pointerSize/2,event.clientY-pointerSize/2);
		// }
		//
		// window.addEventListener("pointermove",onPointerMove);
		//
		// function onPointerUp (event) {
		// 	HideDiv(event.pointerId);
		// }
		//
		// window.addEventListener("pointerup",onPointerUp);


	}

	public createEventDict(json: TuioMessage): PointerEventInit {
		const screenX = this.divWith * json.xPos;
		const screenY = this.divHeight * json.yPos;
		const clientX = screenX;
		const clientY = screenY;

		return {
			pointerId: json.cursorId + 1, //because 0 seems to be an invalid id as not written anywhere in docs ಠ_ಠ
			clientX: clientX,
			clientY: clientY,
			screenX: screenX,
			screenY: screenY,
			pointerType: 'touch',
			bubbles: true,
			cancelable: true,
			composed: true,
			isPrimary: json.cursorId === 0,
		};
	}

	public LaunchTouchEvent(json: TuioMessage, event: string) {
		const pointerEventInit: PointerEventInit = this.createEventDict(json);

		const pointerEvent = new PointerEvent(event, pointerEventInit);
		let pointedElement = document.elementFromPoint(
			pointerEventInit.clientX as number,
			pointerEventInit.clientY as number,
		);

		if (!pointedElement) {
			console.error(`No pointed element at [${pointerEventInit.clientX},${pointerEventInit.clientY}] ?`);
			return;
		}

		if (event == 'pointerdown') {
			const pointerEnterEvent = new PointerEvent('pointerenter', pointerEventInit);
			pointedElement.dispatchEvent(pointerEnterEvent);
			this.pointerInfos.set(json.cursorId, pointedElement);
			pointedElement.dispatchEvent(pointerEvent);
			return;
		}

		if (event == 'pointermove') {
			let previousPointedElement = this.pointerInfos.get(json.cursorId);
			if (previousPointedElement) {
				previousPointedElement.dispatchEvent(pointerEvent);
			}
		}

		if (event == 'pointerup') {
			let previousPointedElement = this.pointerInfos.get(json.cursorId);
			if (previousPointedElement) {
				previousPointedElement.dispatchEvent(pointerEvent);
			}
			this.pointerInfos.delete(json.cursorId);
		}
	}

}


export const dragPointers:Map<number, boolean> = new Map<number,boolean>();
