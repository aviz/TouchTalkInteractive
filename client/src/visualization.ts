import {divs, WildDiv} from "./wildDiv";
import {Sync} from "./wildSync";

const month = ["Jan","Feb","Mar","Apr","May","Jun",
			   "Jul","Aug","Sep","Oct","Nov","Dec"];

export interface iVisualization {
	id:number;
}

export class Visualization extends WildDiv {
  	static svgNS = 'http://www.w3.org/2000/svg';

	contentImg:HTMLImageElement|null=null;
  	public svg: SVGElement | undefined;
  	private g: SVGGElement | undefined;

	public strokeWidth = 1.5;
	public strokeColor = '#DDD';
	public opacity = 0.85;
	public interval = 22;
	public offset = 4;
  	public yValue = 7;
  	public clicked:null | ((number, boolean) => void) = null;

	constructor(id:number) {
		super(id);
	}

	async load(div : HTMLDivElement, x:number, y:number, width:number, height:number, url:string) {

		super.init(div, x, y, width, height);

    	this.div!.style.background = 'rgba(84, 84, 86, 0.8)';

		//Create image
		this.contentImg = document.createElement('img');
		this.contentImg.src="data/" + url;
		this.contentImg.className='dataImg';
		this.div!.appendChild(this.contentImg);
		this.contentImg.addEventListener('pointerdown', (event) => {
			event.preventDefault();
		})

		// Add the time axis
	    if (this.svg) this.svg.remove();
	    this.svg = document.createElementNS(Visualization.svgNS, 'svg') as SVGElement;
		this.svg.style.display = 'block'
	    this.svg.style.height = '20px';
	    this.svg.style.userSelect = 'none';

	    this.div!.appendChild(this.svg);
	    this.g = document.createElementNS(Visualization.svgNS, 'g') as SVGGElement;
	    this.g.setAttribute('opacity', String(this.opacity));
	    this.svg.appendChild(this.g);

	    // Add interactive line
	    const path = document.createElementNS(Visualization.svgNS, 'line') as SVGLineElement;
	    path.setAttribute('x1', this.interval/2 + '');
	    path.setAttribute('y1', 0 + 'px');
	    path.setAttribute('x2', 13 * this.interval + '');
	    path.setAttribute('y2', 0 + 'px');
	    path.setAttribute('fill', '#FFFFFF');
	    path.setAttribute('stroke-width', String(this.strokeWidth));
	    path.setAttribute('stroke', this.strokeColor);
	    path.addEventListener('pointerdown', (event) => {
	      console.log("You touched the timeline")
	      event.stopPropagation();
	    });
	    path.addEventListener('pointerup', (event) => {
	      // finger away
	      console.log("You stopped touching the timeline")
	    });
	    path.id = '_timeline';
	    this.g.appendChild(path);
	}

  	// Add clickable month names
  	public addMonthLabels (clicked: (number, boolean) => void) {

	    this.clicked = clicked;

	    for (let i = 0; i < 12; i++) {
	      // Left labels
	      const text = document.createElementNS(Visualization.svgNS, 'text') as SVGTextElement;
	      text.innerHTML = month[i];
	      text.setAttribute('text-anchor', 'middle');
	      text.setAttribute('font-size', `4px`);
	      text.setAttribute('x', String(this.strokeWidth + (i+1)*this.interval - this.offset));
	      text.setAttribute('y', this.yValue + 'px');
	      text.setAttribute('fill', 'rgb(48, 219, 91)');

	      text.addEventListener('pointerdown', (event) => {
	        console.log("You touched the month " + month[i]);
	        if(this.clicked) {
	          this.clicked(i, true);
	        }
	        event.stopPropagation();
	      });
	      text.id = '_month_' + i;

	      this.g!.appendChild(text);

	      // Right labels
	      const textRight = document.createElementNS(Visualization.svgNS, 'text') as SVGTextElement;
	      textRight.innerHTML = month[i];
	      textRight.setAttribute('text-anchor', 'middle');
	      textRight.setAttribute('font-size', `4px`);
	      textRight.setAttribute('x', String(this.strokeWidth + (i+1)*this.interval + this.offset));
	      textRight.setAttribute('y', this.yValue + 'px');
	      textRight.setAttribute('fill', 'rgb(64, 156, 255)');

	      textRight.addEventListener('pointerdown', (event) => {
	        console.log("You touched the month " + month[i]);
	        if(this.clicked) {
	          this.clicked(i, false);
	        }
	        event.stopPropagation();
	      });
	      textRight.id = '_month_right_' + i;

	      this.g!.appendChild(textRight);
	    }
	}

}
