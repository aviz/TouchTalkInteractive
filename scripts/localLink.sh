#!/bin/bash

#connexion prameters
httpServerIp="127.0.0.1"
httpServerPort=8080
syncServerIp="127.0.0.1"
syncServerPort=8081
tuioport=8083

#client offset in pixel
wallOffsetX=1440
wallOffsetY=480
#client screen size
clientCanvasWidth=1440
clientCanvasHeight=480

#master canvas size. MUST be the same ratio than the wall
masterscreenwidth=1440
masterscreenheight=480


#wall total size
wallwidth=2880
wallheight=960

wallRatio=2


echo "start nw client"

echo "Client web page:" http://$httpServerIp:$httpServerPort?id=a1\&wallOffsetX=$wallOffsetX\&wallOffsetY=$wallOffsetY\&canvasWidth=$clientCanvasWidth\&canvasHeight=$clientCanvasHeight\&syncServerIp=$syncServerIp\&syncServerPort=$syncServerPort\&wallRatio=$wallRatio

echo "Leader web page:" http://$httpServerIp:$httpServerPort?id=MASTER\&canvasWidth=$masterscreenwidth\&canvasHeight=$masterscreenheight\&syncServerPort=$syncServerPort\&syncServerIp=$syncServerIp\&tuioServerPort=$tuioport\&tuioServerIp=$syncServerIp
