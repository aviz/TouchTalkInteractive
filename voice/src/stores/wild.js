import { readable } from 'svelte/store';
import io from "socket.io-client";
import { autoType } from 'd3';

const defaultWallConfig = {
	id: 'MASTER',
	syncServerIp: '127.0.0.1',
	syncServerPort: 8080,
	tuioServerIp: '127.0.0.1',
	tuioPort: '8083',
	offsetX: 0,
	offsetY: 0,
	sizeX:800,
	sizeY:600,
	socketViewSync:null,
	isMaster:false,
	wallWidth:800,
	wallHeight:600
};

export const wallConfig = readable({}, set => {
	const urlParamsObj = new URLSearchParams(window.location.search);
	const urlParams = Object.fromEntries(urlParamsObj);
	const completeParams = {
		...defaultWallConfig,
		...autoType(urlParams)
	};

	completeParams.socketViewSync = io(`http://${completeParams.syncServerIp}:${completeParams.syncServerPort}`);
	completeParams.socketViewSync.emit('ID',completeParams.id);
	completeParams.socketViewSync.on('Rpc', (funcToCall) => {
		console.log("Received RPC "+funcToCall.func );
		console.log(funcToCall.args );
		window[funcToCall.func].apply(this, funcToCall.args);
	});
	if (completeParams.id == "MASTER")
		completeParams.isMaster = true;

	set(completeParams);

});
