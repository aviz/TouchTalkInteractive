import { readable } from 'svelte/store';
import { autoType } from 'd3';

const defaultParams = {
    config: 'config.json',
    sources: 'Interactive'
};

export const params = readable({}, set => {
    const urlParamsObj = new URLSearchParams(window.location.search);
    const urlParams = Object.fromEntries(urlParamsObj);
    const completeParams = {
        ...defaultParams,
        ...autoType(urlParams)
    };
    set(completeParams);
});
