import express from "express";
import socketio from "socket.io";
// @ts-ignore
import * as Tuio from "tuio-nw";
import {OneEuroFilter} from "./OneEuroFilter";
import chalk, {blue} from "chalk";
import commandLineArgs from "command-line-args";
import {Server} from 'node-osc';
import MiniSearch from 'minisearch'
import {promises as fs} from "fs";
import { createLogger, transports, format } from "winston";



const optionDefinitions = [
	{name: "ip", alias: "i", type: String},
	{name: "width", alias: "w", type: Number, defaultValue: 5},
	{name: "height", alias: "h", type: Number, defaultValue: 2},
	{name: "syncport", type: Number, defaultValue: 8081},
	{name: "tuioinport", type: Number, defaultValue: 3333},
	{name: "tuiooutport", type: Number, defaultValue: 8083},
	{name: "oscinport", type: Number, defaultValue: -1},
	{name: "tracelevel", type: Number, defaultValue: 1},
];

/*------------------------------------------*/
/* Logging                                  */

/*------------------------------------------*/
enum LogType {
	PLAIN = 0,
	INFO = 1,
	WARNING = 2,
	ERROR = 3,
}

let TRACE_LEVEL = 0;

const date = new Date();


const logger = createLogger({
	transports: [
		new transports.File({
			dirname: "logs",
			filename: `AskWilder-${date.getFullYear()}-${date.getMonth()}-${date.getDate()}-${date.getHours()}-${date.getMinutes()}-${date.getSeconds()}.log`,
		}),
	],
	format: format.combine(
		format.timestamp(),
		format.printf(({ timestamp, level, message, service }) => {
			return `[${timestamp}] ${service} ${level}: ${message}`;
		})
	),
	defaultMeta: {
		service: "",
	},
});



const options = commandLineArgs(optionDefinitions);
TRACE_LEVEL = options.tracelevel;
console.log("Command line " + process.argv);
console.log("ip " + options.ip);
console.log("width " + options.width);
console.log("height " + options.height);
console.log("syncport " + options.syncport);
console.log("tuioinport " + options.tuioinport);
console.log("tuiooutport " + options.tuiooutport);
console.log("oscinport " + options.oscinport);

/*------------------------------------------*/
/* Synch                                    */
/*------------------------------------------*/

const appSync = express();
const viewSyncPort = options.syncport;
const viewSyncServer = appSync.listen(viewSyncPort, "0.0.0.0", () => {
	console.log(
		`Sync server listening on ${options.ip}:${viewSyncPort}`,
		1,
		LogType.INFO
	);
});
const viewSyncSocket = socketio.listen(viewSyncServer);
let masterSocket: socketio.Socket;
let miniSearch : MiniSearch;

// whenever a client connects on port via
// a websocket, log that a client has connected
viewSyncSocket.on("connect", (socket: socketio.Socket) => {
	socket.on("ID", (message) => {
		if (message === "MASTER") {
			console.log("Master listening for updates");
			masterSocket = socket;
			socket.on("Rpc", (message) => {
				// directly forward viewport transform commands to all clients
				// console.log("Rpc -> "+JSON.stringify(message));
				//Forward message to slaves
				socket.broadcast.emit("Rpc", message);
			});
			initSearchEngine().then(() => {

				if (!miniSearch) {
					miniSearch = new MiniSearch({
						fields: ['title', 'content'], // fields to index for full-text search
						storeFields: ['id'] // fields to return with search results
					});

					// Index all documents
					miniSearch.addAll(jsons);
				}

			});

		} else {
			console.log(`Slave ${message} listening for updates`);
		}
	});
	socket.on("SEARCH", (message:any) => {
		console.log(message.id + ' Searching ' + message.keyword);
		if (miniSearch) {
			// @ts-ignore
			let results = miniSearch.search(message.keyword,{ prefix: true });
			console.log(results);

			masterSocket.emit("RESULT", {id:message.id, keyword:message.keyword, results:results});
		}
	});
	socket.on("ERASE", (message:any) => {
		console.log(message.id + ' clearing search');
		if (miniSearch) {
			masterSocket.emit("RESULT", {id:message.id, keyword:'', results:[]});
		}
	});
	socket.on("VOICE", (message:any) => {
		console.log(message);
		if (masterSocket) {
			// @ts-ignore
			masterSocket.emit("Rpc", message);
		}
	});
	socket.on("LOG", (message:any) => {
		console.log("LOG" + message);
		if (masterSocket)
			logger.info(message)
	});

});




/*------------------------------------------*/
/* TUIO                                    */
/*------------------------------------------*/

const EPSILON = 0.001;

interface filterData {
	filter: OneEuroFilter;
	lastTimeStamp: number;
	lastValue: number;
}

const filtersX: Array<filterData> = [];
const filtersY: Array<filterData> = [];

for (let i = 0; i < 32; i++) {
	const fx: OneEuroFilter = new OneEuroFilter(60, 1, 1, 1);
	const dataX: filterData = {filter: fx, lastTimeStamp: 0, lastValue: 0};
	filtersX.push(dataX);
	const fy: OneEuroFilter = new OneEuroFilter(60, 1, 1, 1);
	const dataY: filterData = {filter: fy, lastTimeStamp: 0, lastValue: 0};
	filtersY.push(dataY);
}

interface tuioMessage {
	xPos: number;
	yPos: number;
	cursorId: number;
}

function filter(msg: tuioMessage) {
	if (
		!isNaN(msg.yPos) &&
		!isNaN(msg.xPos) &&
		msg.cursorId >= 0 &&
		msg.cursorId < 32
	) {
		const timestamp = new Date().getTime() * 0.001;
		if (Math.abs(timestamp - filtersX[msg.cursorId].lastTimeStamp) < EPSILON)
			msg.xPos = filtersX[msg.cursorId].lastValue;
		else {
			msg.xPos = filtersX[msg.cursorId].filter.filter(msg.xPos, timestamp);
			filtersX[msg.cursorId].lastValue = msg.xPos;
		}
		filtersX[msg.cursorId].lastTimeStamp = timestamp;
		if (Math.abs(timestamp - filtersY[msg.cursorId].lastTimeStamp) < EPSILON)
			msg.yPos = filtersY[msg.cursorId].lastValue;
		else {
			msg.yPos = filtersY[msg.cursorId].filter.filter(msg.yPos, timestamp);
			filtersY[msg.cursorId].lastValue = msg.yPos;
		}
		filtersY[msg.cursorId].lastTimeStamp = timestamp;
	}
}

function resetFilter(id: number) {
	filtersX[id].filter.reset();
}

const appTuio = express();
appTuio.set("port", options.tuiooutport);

const http = require("http").Server(appTuio);
const ioTuio = socketio(http);

ioTuio.on("connection", () => {
	console.log("TUIO client connected");
});

//Listen and forward TUIO cursor events
const tuioClient = new Tuio.Client({
	host: options.ip,
	port: options.tuioinport,
});

function onAddTuioCursor(addCursor: tuioMessage) {
	//resetFilter(addCursor.cursorId);
	//filter(addCursor);
	ioTuio.emit("onAdd", addCursor);
	console.log(addCursor);
}

function onUpdateTuioCursor(updateCursor: tuioMessage) {
	//filter(updateCursor);
	ioTuio.emit("onUpdate", updateCursor);
}

function onRemoveTuioCursor(removeCursor: tuioMessage) {
	//filter(removeCursor);
	ioTuio.emit("onRemove", removeCursor);
}

tuioClient.on("addTuioCursor", onAddTuioCursor);
tuioClient.on("updateTuioCursor", onUpdateTuioCursor);
tuioClient.on("removeTuioCursor", onRemoveTuioCursor);
tuioClient.listen();

http.listen(options.tuiooutport, () => {
	console.log(`Tuio server listening on ${options.ip}:${options.tuiooutport}`);
});

/*------------------------------------------*/
/* OSC                                      */
/*------------------------------------------*/
if (options.oscinport != -1) {
	//Start osc listener
	const oscServer = new Server(options.oscinport, '0.0.0.0', () => {
		console.log(`OSC Server is listening on port ${options.oscinport}`);

	});

	oscServer.on('message', function (msg) {
		console.log(`Message: ${msg}`);
		if (masterSocket)
			masterSocket.emit("Osc", msg);
	});
}


const articlesFiles = ['1101162143775.json'
	];
const jsons = new Array();

async function initSearchEngine() {

	await Promise.all(articlesFiles.map(async (path): Promise<string> => {
		const fullpath = '../client/src/data/' + path;
		console.log("Loading " + fullpath);
		const jsonFile = await fs.readFile(fullpath, "utf-8");
		const json = JSON.parse(jsonFile);
		jsons.push(json);
		return json;
	}));
}
