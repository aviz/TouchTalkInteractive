# WildSyncServer

Synchronisation and Tuio forwarding for Cluster-Driven Ultra-high-resolution Wall Displays.

## Installation

```npm install```

```npm run build```

## Run
```node lib/server.js  "--ip" "192.168.0.107" ```

## Parameters

  - ip: ip address to bind
  - syncport: websocket port for synchronizing messages
  - tuioinport: port to listen for TUIO messages
  - tuiooutport: websocket port to forward TUIO messages
  - oscinport: port to listen for OSC messages
  - width: width of the TUIO surface for ratio
  - height: height of the TUIO surface for ratio
